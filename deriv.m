function xd = deriv(xv)
% deriv finds & returns the derivative vector of two satellites in relative 
% motion at a point
%
% DESCRIPTION: This function takes the Cartesian state (x, y, z, vx, vy,
% vz) of a vehicle as input and returns the derivatives (xdot, ydot, zdot,
% vxdot, vydot, vzdot) of these states.
%
% INPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% xv           double  1x6     Array containing state of vehicle (x, y, z, 
%                              vx, vy, vz) (km and km/s)
%
% OUTPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% xd           double  1x6     Array containing derivatives of vehicle
%                              state (xdot, ydot, zdot, vxdot, vydot,
%                              vzdot)  (km/s and km/s/s)
%
% ASSUMPTIONS:  (none)
%
% LIMITATIONS:
% (1)  Values for gravitational constant (mu), radius of Chief orbit (Rc), 
%      and mean motion of Chief (n) are set as global variables elsewhere.
%
% SEE ALSO:  (none)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set output defaults (if needed)
% Commentary:
% (none)

%% Argument checking
% Commentary:
% (none)

%% Main Algorithm

%---Set global variables:
global mu Rc n;

%---Set state vector:
x = xv(1);
y = xv(2);
z = xv(3);
vx = xv(4);
vy = xv(5);
vz = xv(6);

Rd = sqrt((Rc+x)^2 + y^2 + z^2);  %---Instantaneous radius of Deputy orbit

%---Compute state derivatives:
xdot = vx;
ydot = vy;
zdot = vz;
vxdot = 2*n*vy + n^2*x - mu*((Rc+x)/(Rd^3) - 1/(Rc^2));
vydot = - 2*n*vx + n^2*y - mu*y/(Rd^3);
vzdot = - mu*z/(Rd^3);

%---Form output derivative array:
xd = [xdot ydot zdot vxdot vydot vzdot];

return