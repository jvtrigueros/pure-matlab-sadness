function [alpha_mean,B] = alpha_filter_3D_dot1stMeas(alpha,svect,IC_theta,IC_phi,unit_1stMeas_direction)
% alpha_filter_3D takes alpha (scale factor) values, eigenvector values, 
% and LOS angles at t=0 and filters out alphas with the wrong sign 
%
% DESCRIPTION: This function takes an array of alpha (scale factor) values,
% an array of eigenvector values from the SVD of the A matrix, and the LOS
% observation angles (theta and phi) at t0 as input and filters out alpha
% values with the wrong sign.  The function returns an array containing
% only alpha values with the correct sign and also returns the mean of
% those alpha values.
%
% INPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% alpha        double  27x1    Scale factor values
% svect        double  27x1    Eigenvectors from SVD of the A matrix
% IC_theta     double  1x1     Observation angle theta at t0 (rad)
% IC_phi       double  1x1     Observation angle phi at t0 (rad)
% unit_1stMeas_direction
%              double  3x1     unit vector in direction of target at time
%                                of actual first/reference observation
%
% OUTPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% alpha_mean   double  1x1     Mean value of scale factor accounting only
%                              for scale factors with the correct sign
% B            double  Nx1     Array containing scale factor values that
%                              are of the correct sign
%
% ASSUMPTIONS:  (none)
%
% LIMITATIONS:  (none)
%
% SEE ALSO:  (none)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set output defaults (if needed)
% Commentary:
% (none)

%% Argument checking
% Commentary:
% (none)

%% Main Algorithm

%---Initialize variables:
x = svect(1);
y = svect(2);
z = svect(3);
index = 1;
B = [NaN];

%---Cycle through alpha values:
%   eigenvector components to determine if alpha has the correct sign.  If
%   it does, count it and add it to the output array B that contains all
%   alpha values with the correct sign:
for i = 1:length(alpha)
    
    %---Compute dot product of alpha*singular_vector and unit vector in
    %   direction of first measurement:
    dot_prod_result = dot(alpha(i)*[x; y; z],unit_1stMeas_direction);

    %---If dot product is positive, accept this alpha value and update the
    %   array B which is used to compute the mean of only the alpha
    %   values with the correct sign:
    if dot_prod_result > 0
        B(index,1) = alpha(i);
        index = index+1;
    end

end

%---Compute mean of alpha values with correct sign:
alpha_mean = mean(B);
