%% Process data after filtering alpha values
    alphaSV_filtered = zeros(27,27);
    alphaSVmean_filtered = zeros(27);
    solvedIC_SV_filtered = zeros(6,27);
    x_vol_SV_filtered = zeros(num,3,27);
    theta_SV_filtered = zeros(num,27);
    phi_SV_filtered = zeros(num,27);
    resSVtheta_filtered = zeros((num-1),27);
    resSVphi_filtered = zeros((num-1),27);
    RMSerr_SV_filtered = zeros(1,27);
    alphaSV_length_filtered = zeros(1,27);
      
for SV_num = 1:27
    
    singvecs = V(:,SV_num);  %---SVD vector corresponding to singular vector (SV_num)
    
    %---Filter out alphas with incorrect sign based on observation angle values
    %   and eigenvector values at t0:
    [alphameantemp_filtered,alphaSVtemp_filtered] = alpha_filter_3D_dot1stMeas(alpha(:,SV_num),singvecs(:,1),theta_N(1,1),phi_N(1,1),unit_1stMeas_direction);

    alphaSVmean_filtered(SV_num) = alphameantemp_filtered;  %---Create array containing mean alpha values
    
    alphaSV_length_filtered(SV_num) = length(alphaSVtemp_filtered);
    
    for icount = 1:length(alphaSVtemp_filtered)
        alphaSV_filtered(icount,SV_num) = alphaSVtemp_filtered(icount); %---Store alpha values for each singular vector
    end
    
    %---Compute solved ICs with filtered mean alpha value:
    solvedIC_SV_filtered(:,SV_num) = alphaSVmean_filtered(SV_num)*V(1:6,SV_num);
   
    %---Use QV relative motion equations to determine QV-predicted states at
    %   measurement times:
    x_vol_SV_filtered(:,:,SV_num) = vol_sol(t,n,Rc,solvedIC_SV_filtered(1,SV_num),solvedIC_SV_filtered(2,SV_num),solvedIC_SV_filtered(3,SV_num),solvedIC_SV_filtered(4,SV_num),solvedIC_SV_filtered(5,SV_num),solvedIC_SV_filtered(6,SV_num));
    
    %---Compute predicted observation angles using QV solution results at
    %   measurement times:
    theta_SV_filtered(:,SV_num) = atan2(x_vol_SV_filtered(:,1,SV_num),x_vol_SV_filtered(:,2,SV_num));
    phi_SV_filtered(:,SV_num)   = atan2(x_vol_SV_filtered(:,3,SV_num),x_vol_SV_filtered(:,2,SV_num));
  
            
    %---Compute the residuals of the observation angles found using the
    %   computed ICs found and the filtered alpha values for the specified
    %   singular vector (NOTE: Starting at the index value "2" skips the
    %   first value in the theta_N and phi_N arrays which actually corresponds
    %   to the initial state, not the first measurement.):
    resSVtheta_filtered(:,SV_num) = theta_N(2:end) - theta_SV_filtered(2:end,SV_num); % true_theta_noise - QV_theta
    resSVphi_filtered(:,SV_num)   = phi_N(2:end) - phi_SV_filtered(2:end,SV_num);     % true_phi_noise   - QV_phi
    
    %---Resolve observation angle residuals to the range -pi to pi:
    for i=1:(num-1)
        if resSVtheta_filtered(i,SV_num) > pi
            resSVtheta_filtered(i,SV_num) = resSVtheta_filtered(i,SV_num)-2*pi;
        elseif resSVtheta_filtered(i,SV_num) < -pi
            resSVtheta_filtered(i,SV_num) = resSVtheta_filtered(i,SV_num)+2*pi;
        end
        if resSVphi_filtered(i,SV_num) > pi
            resSVphi_filtered(i,SV_num) = resSVphi_filtered(i,SV_num)-2*pi;
        elseif resSVphi_filtered(i,SV_num) < -pi
            resSVphi_filtered(i,SV_num) = resSVphi_filtered(i,SV_num)+2*pi;
        end
    end
    
    %---Compute total RMS error (QV mean alpha filtered solution compared to QV with IC) of
    %   angle residuals:
    RMSerr_SV_filtered(1,SV_num) = sqrt((sum(resSVtheta_filtered(:,SV_num).^2,1)+sum(resSVphi_filtered(:,SV_num).^2,1))/(length(resSVtheta_filtered(:,SV_num))+length(resSVphi_filtered(:,SV_num))));

end