function x_vol = vol_sol(t,n,R,x,y,z,xdot,ydot,zdot)
% vol_sol uses the Quadratic Volterra (QV) relative motion equations to
% compute the relative position of a spacecraft at a future time
%
% DESCRIPTION: This function takes the state of a Deputy vehicle as input 
% along with the mean motion and radius of the Chase vehicle orbit and 
% computes the state of the Deputy at a specified future time using the
% Quadratic Volterra relative motion equations.  The coordinate system used
% is the radial, transverse, normal system with the following directions
% assumed for each axis:
%
%    +X = + radial direction (away from central body)
%    +Y = +V-bar direction (direction of velocity - completes the 
%           right-handed coordinate system with the X and Z axes)
%    +Z = + angular momentum vector direction
%
% INPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% t            double  1x1     Future time at which Deputy relative state 
%                              is desired (time unit ... typically,
%                              seconds)
% n            double  1x1     Mean motion of Chief orbit (rad/[time unit]
%                              ... typically, rad/s)
% R            double  1x1     Radius of Chief orbit (distance unit ...
%                              typically meters or kilometers)
% x            double  1x1     X component of relative position at initial
%                              time (distance unit ... typically meters or
%                              kilometers)
% y            double  1x1     Y component of relative position at initial
%                              time (distance unit ... typically meters or
%                              kilometers)
% z            double  1x1     Z component of relative position at initial
%                              time (distance unit ... typically meters or
%                              kilometers)
% xdot         double  1x1     X component of relative velocity at initial
%                              time (distance unit/time unit ... typically 
%                              meters/s or kilometers/s)
% ydot         double  1x1     Y component of relative velocity at initial
%                              time (distance unit/time unit ... typically 
%                              meters/s or kilometers/s)
% zdot         double  1x1     Z component of relative velocity at initial
%                              time (distance unit/time unit ... typically 
%                              meters/s or kilometers/s)
%
% OUTPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% x_vol        double  1x3     Array containing position (x y z) of 
%                              Deputy at specified future time (distance
%                              unit ... typically meters or kilometers)
%
% ASSUMPTIONS:
% (1)  All time, distance, angular rate, and speed units used for input 
%      variables are consistent.
%
% LIMITATIONS:  (none)
%
% SEE ALSO:  (none)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set output defaults (if needed)
% Commentary:
% (none)

%% Argument checking
% Commentary:
% (none)

%% Main Algorithm

%   Reference:  "A SECOND ORDER METHOD FOR INITIAL RELATIVE ORBIT
%   DETERMINATION USING ANGLES-ONLY OBSERVATIONS" - Pratt, Lovell, Newman -
%   AAS 14-293

%---Propagate initial conditions using the quadratic Volterra solution

xt = (4-3*cos(n*t))*x + (1/n*sin(n*t))*xdot + (2/n*(1-cos(n*t)))*ydot;

yt = (6*(sin(n*t)-n*t))*x + y + (2/n*(-1+cos(n*t)))*xdot + (1/n*(4*sin(n*t)-3*n*t))*ydot;

zt = cos(n*t)*z + (1/n *sin(n*t))*zdot;

%---Compute the radial position component:
xvol = xt + (3/(2*R)*(7-10*cos(n*t)+3*cos(2*n*t)+12*n*t.*sin(n*t)-12*n^2*t.^2))*x^2 ...
    + (3/(2*R)*(1-cos(n*t)))*y^2 ...
    + (1/(4*R)*(3-2*cos(n*t)-cos(2*n*t)))*z^2 ...
    + (1/(2*n^2*R)*(-3+4*cos(n*t)-cos(2*n*t)))*xdot^2 ...
    + (1/(2*n^2*R)*(6-10*cos(n*t)+4*cos(2*n*t)+12*n*t.*sin(n*t)-9*n^2*t.^2))*ydot^2 ...
    + (1/(4*n^2*R)*(3-4*cos(n*t)+cos(2*n*t)))*zdot^2 ...
    + 2*(3/R*(-sin(n*t)+n*t))*x*y ...
    + 2*(3/(2*n*R)*(4*sin(n*t)-sin(2*n*t)-4*n*t+2*n*t.*cos(n*t)))*x*xdot ...
    + 2*(3/(2*n*R)*(4-6*cos(n*t)+2*cos(2*n*t)+7*n*t.*sin(n*t)-6*n^2*t.^2))*x*ydot ...
    + 2*(3/(2*n*R)*(-sin(n*t)+n*t))*y*ydot ...
    + 2*(1/(4*n*R)*(2*sin(n*t)-sin(2*n*t)))*z*zdot ...
    + 2*(1/(2*n^2*R)*(7*sin(n*t)-2*sin(2*n*t)-6*n*t+3*n*t.*cos(n*t)))*xdot*ydot;

%---Compute the transverse position component:
yvol = yt + (3/(4*R)*(40*sin(n*t)+3*sin(2*n*t)-22*n*t-24*n*t.*cos(n*t)))*x^2 ...
    + (3/R*(sin(n*t)-n*t))*y^2 ...
    + (1/(4*R)*(4*sin(n*t)+sin(2*n*t)-6*n*t))*z^2 ...
    + (1/(4*n^2*R)*(8*sin(n*t)-sin(2*n*t)-6*n*t))*xdot^2 ...
    + (1/(n^2*R)*(10*sin(n*t)+sin(2*n*t)-6*n*t-6*n*t.*cos(n*t)))*ydot^2 ...
    + (1/(4*n^2*R)*(8*sin(n*t)-sin(2*n*t)-6*n*t))*zdot^2 ...
    + 2*(3/(2*R)*(1-cos(n*t)))*x*y ...
    + 2*(3/(4*n*R)*(-5+4*cos(n*t)+cos(2*n*t)+4*n*t.*sin(n*t)))*x*xdot ...
    + 2*(3/(2*n*R)*(12*sin(n*t)+sin(2*n*t)-7*n*t-7*n*t.*cos(n*t)))*x*ydot ...
    + 2*(3/(2*n*R)*(-sin(n*t)+n*t))*y*xdot ...
    + 2*(1/(4*n*R)*(-3+4*cos(n*t)-cos(2*n*t)))*z*zdot ...
    + 2*(1/(2*n^2*R)*(-3+2*cos(n*t)+cos(2*n*t)+3*n*t.*sin(n*t)))*xdot*ydot;

%---Compute the normal position component:
zvol = zt + 2*(3/(4*R)*(-3+2*cos(n*t)+cos(2*n*t)+4*n*t.*sin(n*t)))*x*z ...
    + 2*(3/(4*n*R)*(2*sin(n*t)+sin(2*n*t)-4*n*t.*cos(n*t)))*x*zdot ...
    + 2*(1/(4*n*R)*(2*sin(n*t)-sin(2*n*t)))*z*xdot ...
    + 2*(1/(2*n*R)*(-3+2*cos(n*t)+cos(2*n*t)+3*n*t.*sin(n*t)))*z*ydot ...
    + 2*(1/(4*n^2*R)*(3-4*cos(n*t)+cos(2*n*t)))*xdot*zdot ...
    + 2*(1/(2*n^2*R)*(sin(n*t)+sin(2*n*t)-3*n*t.*cos(n*t)))*ydot*zdot;

%---Assemble the output vector containing the X, Y, and Z position
%   components:
x_vol = [xvol yvol zvol];
end