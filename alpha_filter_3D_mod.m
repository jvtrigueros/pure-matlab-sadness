function [alpha_mean,B] = alpha_filter_3D_mod(alpha,svect,IC_theta,IC_phi)
% alpha_filter_3D_mod performs NO alpha (scale factor) filtering 
%
% DESCRIPTION: This function is a "dummy" function and performs no alpha
% value filtering.
%
% INPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% alpha        double  27x1    Scale factor values
% svect        double  27x1    Eigenvectors from SVD of the A matrix
% IC_theta     double  1x1     Observation angle theta at t0 (rad)
% IC_phi       double  1x1     Observation angle phi at t0 (rad)
%
% OUTPUTS:
% VARIABLE     TYPE    SIZE    DESCRIPTION (Optional/Default)
% alpha_mean   double  1x1     Mean value of scale factor accounting only
%                              for scale factors with the correct sign
% B            double  Nx1     Array containing scale factor values that
%                              are of the correct sign
%
% ASSUMPTIONS:  (none)
%
% LIMITATIONS:  (none)
%
% SEE ALSO:  (none)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set output defaults (if needed)
% Commentary:
% (none)

%% Argument checking
% Commentary:
% (none)

%% Main Algorithm

%---Initialize variables:
B = alpha;

%---Compute mean of alpha values:
alpha_mean = mean(B);
