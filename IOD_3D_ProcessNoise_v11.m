
% The IOD_3D ProcessNoise_v11 is an updated version of IOD_3D_ProcessNoise_v06 
% where the user can apply the IOD solution method with or without process noise 
% (i.e., the simulated measurements can be generated using the propagated two-body 
% solution or the QV relative motion equations). This version 
% includes the following modifications:
%
% (1) Allows the user to specify whether the code is being used to process
%     simulated data or real flight data.  If real flight data is being
%     processed, certain metrics used for performance evaluation are not
%     computed since "truth" data is not available.
%
% DESCRIPTION: This script takes 15 simulated line-of-sight (LOS) angle 
% measurements taken by a Chief vehicle of a Deputy vehicle and processes 
% those measurements to determine the initial relative state (position and
% velocity) of the Deputy with respect to the Chief in the RSW frame.  The
% simulated measurements are read from an external file and are generated 
% using the propagated two-body relative motion equations.  Noise can be added 
% to the simulated measurements and 3D relative motion is modeled.
%
% 3D IOD method with process noise [28 x 27]
%
% MODIFICATION HISTORY
%2015/07/01
%   1. Included comparison plots for the "true" trajectory and the QV 
%      propagated trajectory with each of the solved ICs
%   2. Included the RMSE for position
%   3. Provide ECI state of the deputy

% 2015/05/11 E. Pinon III
%   1.  Fixed error that attempted to load a nonexistent file.
%   2.  Added ability for the user to choose the observation data file that
%       will be processed.
%   3.  Cleaned up the output written to the screen and output file.
%
% 2015/05/04 E. Pinon III
%   1.  Modified code to allow the user to specify whether simulated or
%       real data is being processed.  If real data is being processed, the
%       code will not attempt to compute metrics and other quantities that
%       depend on knowledge of "truth" data.
%
% 2015/04/28 E. Pinon III
%   1.  Modified code to cycle through all singular vectors and perform a
%       line search for the alpha value that minimizes the RMS error for
%       each singular vector
%
% 2015/04/11 E. Pinon III
%   1.  Modified code to store data from the first line of the observation
%       data file created by the Generate_Obs_IOD_3D_ProcessNoise_v07.m
%       script in a separate array as the initial conditions used for
%       two-body propagation of the "truth" trajectory - The next 14 lines
%       of data from the observation data file are treated as measurements
%       needed to solve for the initial conditions using the QV method.
%   2.  Calls alpha_filter_3D_1stMeas that uses the direction of the first
%       measurement to filter out alpha values of the wrong sign in
%       subsequent measurements
%
% INPUTS:
% (none)
%
% OUTPUTS:
% (none)
%
% ASSUMPTIONS:
% (1)  Units for distance are (km) and for velocity are (km/s)
% (2)  Chief orbit is circular
%
% LIMITATIONS:
% (1)  Global variables:
%        (a) mu = gravitational constant (km^3/s^2)
%        (b) Rc = radius of Chief orbit (km)
%        (c) n = mean motion of Chief orbit (rad/s)
%
% SEE ALSO:
%   SVAT\Code\VolterraIRODCode\New_Code\deriv.m
%   SVAT\Code\VolterraIRODCode\New_Code\alpha_filter_3D_1stMeas.m
%   SVAT\Code\VolterraIRODCode\New_Code\vol_sol.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set output defaults (if needed)
% Commentary:
% (none)

%% Argument checking
% Commentary:
% (none)

%% TO-DO List

%% Begin Main Algorithm

clear all
clc
close all
format long;

%---Initialize variables:
global mu Rc n;
mu = 398600;        % Gravitational parameter (km^3/s^2)
Rc = 7100;          % Chief orbit radius (km), semimajor axis
n = sqrt(mu/Rc^3);  % Chief mean motion (rad/s)
e=0;                %Chief eccentricity
i=70*(pi/180); % Chief inclination in radians
arg_peri=0;
arg_node=0*(pi/180);
M0=0;
M_Earth=5.972e24;
R_Earth=6371;
line_search_pts = 1001;  % number of points for line search


%---Ask user what type of data is being processed:
%   (construct a questdlg with three options)
data_selection = questdlg('What type of data is being processed?', ...
	'Data Type Selection', ...
	'Simulated','Real','Cancel','Simulated');

%---Handle response:
switch data_selection
    case 'Simulated'
        disp([data_selection ' data being processed ...']);
        data_type = 1;
    case 'Real'
        disp([data_selection ' data being processed ...']);
        data_type = 2;
    case 'Cancel'
        disp('No data being processed');
        data_type = 0;
        return;
end

disp('Running IOD_3D_ProcessNoise_v11 ...');
fprintf('\n');

%% Read observation data from external file

%--------------------------------------------------------------------------
%---Get name of file that contains observation data:
dialog_title = 'Please select the observation data input file (Example: IOD_3D_Sim_Obs_Noise_v11.txt)';
obs_data_input_filename = uigetfile('*.txt',dialog_title);

%---Open simulated/real observation input file.  NOTE:  When processing
%     real data, this file will contain the timetags and real observation
%     angles (theta and phi).  When processing simulated data, this file
%     will contain the timetags and simulated observation angles that
%     include noise.
fid1 = fopen(obs_data_input_filename,'r'); %---Input file containing camera2body DCM data

%---Set format specification and delimeter to read data:
formatSpec = '%f%f%f%[^\n\r]';
% delimiter = ' ';
delimiter = '\t';


%---Read columns of data according to format string:
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray1 = textscan(fid1, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true,  'ReturnOnError', false);

%---Close the input file:
fclose(fid1);

%---Allocate imported arrays to variables needed for QV calculations:
t = cell2mat(dataArray1(:, 1));        % time
theta_N = round(dataArray1{:, 2},15);  % theta with noise
phi_N = round(dataArray1{:, 3},15);    % phi with noise

num = length(theta_N);  % nubmer of measurements

%% Check to see if the user wants to process simulated data and open and
%     read the appropriate files:
if data_type == 1
    
    %---Open data file containing simulated observation data without noise
    %     (i.e., "truth" data):
    fid2 = fopen('IOD_3D_Sim_Obs_v11.txt','r'); %---Input file containing timetag, theta, and phi (without noise)
    dataArray2 = textscan(fid2, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true,  'ReturnOnError', false);
    
    %---Open data file containing miscellaneous sim parameters (only needed for
    %     simulated data):
    fid3 = fopen('IOD_3D_Misc_Sim_Parameters_v11.txt','r'); %---Input file containing miscellaneous parameters
    
    method = fgetl(fid3);
    num_temp = str2num(fgetl(fid3));
    ti = str2num(fgetl(fid3));
    dtrk = str2num(fgetl(fid3));
    tf = str2num(fgetl(fid3));
    
    fclose(fid2);
    fclose(fid3);
    
    theta = round(dataArray2{:, 2},15);   % theta with no noise
    phi = round(dataArray2{:, 3},15);     % phi with no noise


    %---Open data file containing simulated target vehicle states at the
    %     observation times (only needed for simulated data):

    formatSpec = '%f%f%f%f%f%f%f%[^\n\r]';
    
    fid1 = fopen('IOD_3D_Sim_Obs_States_v11.txt','r'); %---Input file containing timetag, xobs, yobs, and zobs
    
    %---Read columns of data according to format string.
    % This call is based on the structure of the file used to generate this
    % code. If an error occurs for a different file, try regenerating the code
    % from the Import Tool.
    clear dataArray1;clear dataArray2;
    dataArray1 = textscan(fid1, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true,  'ReturnOnError', false);
    
    %---Close the text files:
    fclose(fid1);
    % fclose(fid2);
    

    %---Allocate imported array to column variable names
    %   NOTE:  The first data point in these variables corresponds to the
    %   first actual measurement.
    %VarName1 = dataArray{:, 1};
    x(:,1) = dataArray1{:, 2};
    x(:,2) = dataArray1{:, 3};
    x(:,3) = dataArray1{:, 4};
    x(:,4) = dataArray1{:, 5};
    x(:,5) = dataArray1{:, 6};
    x(:,6) = dataArray1{:, 7};
    
    %---Set relative states using 'true' propagated two-body solution at times
    %   corresponding to observation times:
    xobs = zeros(num,1);
    yobs = zeros(num,1);
    zobs = zeros(num,1);
    
    for mcount = 1:num
        xobs(mcount,1) = x(mcount,1);        % Two-body radial position at times of observation
        yobs(mcount,1) = x(mcount,2);        % Two-body along-track position at times of observation
        zobs(mcount,1) = x(mcount,3);        % Two-body cross-track position at times of observation
    end;
    
    
    %---Create initial condition array - use the state at the time of the first
    %   measurement as the initial state:
    IC = [x(1,1); x(1,2); x(1,3); x(1,4); x(1,5); x(1,6)];
    x0 = IC(1);
    y0 = IC(2);
    z0 = IC(3);
    xdot0 = IC(4);
    ydot0 = IC(5);
    zdot0 = IC(6);
    r0_lvlh=[x0 y0 z0];
    v0_lvlh=[xdot0 ydot0 zdot0];

% Compute Deputy ECI state at t0
options = odeset('RelTol',1e-11,'AbsTol',1e-12);

% Convert classical orbital elements to ECI position / velocity
[chief_r0, chief_v0] = classicToECI(Rc,e,i,arg_peri,arg_node,M0);

%Calculate the chief's initial orbit radius
R0 = norm(chief_r0);

%Calculate the chief's orbital period
chief_period = sqrt( (4 * pi^2 * R0^3) / mu);

% Establish time vector
ti=0;
dt = chief_period / 1000;
timeVec = ti:dt:t(length(t));

%   Use ODE45 and the nonlinear equations of orbital motion to generate the
%   position and velocity of the chief over its entire orbit
[simtime, trueChiefOrbit] = ode45(@orbitalODE,timeVec, [chief_r0';chief_v0'],options);

trueChief_r = trueChiefOrbit(:,1:3);
trueChief_v = trueChiefOrbit(:,4:6);

j = 1;
for i = 1:length(trueChief_r)
    
    localIhat(i,:) = trueChief_r(i,:) / norm(trueChief_r(i,:));
    localKhat(i,:) = cross(trueChief_r(i,:),trueChief_v(i,:)) / norm(cross(trueChief_r(i,:),trueChief_v(i,:)));
    localJhat(i,:) = cross(localKhat(i,:), localIhat(i,:)) / norm(cross(localKhat(i,:), localIhat(i,:)));
    chiefOmega(i,:) = cross(trueChief_r(i,:), trueChief_v(i,:)) / (norm(trueChief_r(i,:)).^2);
    
    %   Note the second indexing variable used - because LVLHtoECI is a
    %   "matrix of matrices," need to increment it up by 2 to get the
    %   "next" 3x3 attitude matrix.
    ECItoLVLH(j:j+2,:) = [localIhat(i,:); 
                          localJhat(i,:); 
                          localKhat(i,:)];
    j = j+3;
end

% Convert initial LVLH states into Inertial coordinates.
dep_r0 = ECItoLVLH(1:3,1:3)' * r0_lvlh';
dep_v0 = ECItoLVLH(1:3,1:3)' * v0_lvlh' + cross(chiefOmega(1,:), dep_r0)';

dep_r0 = dep_r0' + chief_r0;
dep_v0 = dep_v0' + chief_v0;

ECI_x0=[dep_r0(1) dep_r0(2) dep_r0(3)];
ECI_v0=[dep_v0(1) dep_v0(2) dep_v0(3)];

end

%---If real data is being processed, convert theta and phi from initial
%     measurement to components of unit vector (x0,y0,z0) pointing in
%     direction of Deputy with respect to the Chief in the RSW frame:
if data_type == 2
    %---Set initial y to 1 and solve measurement equations for x and z:
    y0 = 1.0;
    x0 = y0 * tan(theta_N(1));
    z0 = y0 * tan(phi_N(1));
end

%---Form unit pointing vector from initial position coordinates (NOTE:
%     This equation is valid for both real and simulated observations):
unit_1stMeas_direction = [x0; y0; z0]/norm([x0; y0; z0]);

%% Begin IOD implementation

%---Preallocate matrices needed to compute the "A" matrix:
a = zeros((num-1),27);
b = zeros((num-1),27);
c = zeros((num-1),27);
A1 = zeros((num-1),27);
A2 = zeros((num-1),27);

%% Build the A matrix using the QV relative motion solution and the
%   simulated observation angles with noise ---
%   NOTE:  The (i+1) index used for t() in the following equations is
%   necessary to avoid using the data associated with the first
%   observation:
for i = 1:(num-1)
    
    %---Compute coefficients for QV x(t) equation:
    b(i,1) = 4-3*cos(n*t(i+1));
    b(i,2) = 0;
    b(i,3) = 0;
    b(i,4) = 1/n*sin(n*t(i+1));
    b(i,5) = 2/n*(1-cos(n*t(i+1)));
    b(i,6) = 0;
    b(i,7) = 3/(2*Rc)*(7-10*cos(n*t(i+1))+3*cos(2*n*t(i+1))+12*n*t(i+1)*sin(n*t(i+1))-12*n^2*t(i+1)^2);
    b(i,8) = 3/(2*Rc)*(1-cos(n*t(i+1)));
    b(i,9) = 1/(4*Rc)*(3-2*cos(n*t(i+1))-cos(2*n*t(i+1)));
    b(i,10) = 1/(2*n^2*Rc)*(-3+4*cos(n*t(i+1))-cos(2*n*t(i+1)));
    b(i,11) = 1/(2*n^2*Rc)*(6-10*cos(n*t(i+1))+4*cos(2*n*t(i+1))+12*n*t(i+1)*sin(n*t(i+1))-9*n^2*t(i+1)^2);
    b(i,12) = 1/(4*n^2*Rc)*(3-4*cos(n*t(i+1))+cos(2*n*t(i+1)));
    b(i,13) = 2*(3/Rc*(-sin(n*t(i+1))+n*t(i+1)));
    b(i,14) = 0;
    b(i,15) = 0;
    b(i,16) = 2*(3/(2*n*Rc)*(4*sin(n*t(i+1))-sin(2*n*t(i+1))-4*n*t(i+1)+2*n*t(i+1)*cos(n*t(i+1))));
    b(i,17) = 2*(3/(2*n*Rc)*(4-6*cos(n*t(i+1))+2*cos(2*n*t(i+1))+7*n*t(i+1)*sin(n*t(i+1))-6*n^2*t(i+1)^2));
    b(i,18) = 0;
    b(i,19) = 0;
    b(i,20) = 2*(3/(2*n*Rc)*(-sin(n*t(i+1))+n*t(i+1)));
    b(i,21) = 0;
    b(i,22) = 0;
    b(i,23) = 0;
    b(i,24) = 2*(1/(4*n*Rc)*(2*sin(n*t(i+1))-sin(2*n*t(i+1))));
    b(i,25) = 2*(1/(2*n^2*Rc)*(7*sin(n*t(i+1))-2*sin(2*n*t(i+1))-6*n*t(i+1)+3*n*t(i+1)*cos(n*t(i+1))));
    b(i,26) = 0;
    b(i,27) = 0;
    
    %---Compute coefficients for QV y(t) equation:
    a(i,1) = 6*(sin(n*t(i+1))-n*t(i+1));
    a(i,2) = 1;
    a(i,3) = 0;
    a(i,4) = 2/n*(-1+cos(n*t(i+1)));
    a(i,5) = 1/n*(4*sin(n*t(i+1))-3*n*t(i+1));
    a(i,6) = 0;
    a(i,7) = 3/(4*Rc)*(40*sin(n*t(i+1))+3*sin(2*n*t(i+1))-22*n*t(i+1)-24*n*t(i+1)*cos(n*t(i+1)));
    a(i,8) = 3/Rc*(sin(n*t(i+1))-n*t(i+1));
    a(i,9) = 1/(4*Rc)*(4*sin(n*t(i+1))+sin(2*n*t(i+1))-6*n*t(i+1));
    a(i,10) = 1/(4*n^2*Rc)*(8*sin(n*t(i+1))-sin(2*n*t(i+1))-6*n*t(i+1));
    a(i,11) = 1/(n^2*Rc)*(10*sin(n*t(i+1))+sin(2*n*t(i+1))-6*n*t(i+1)-6*n*t(i+1)*cos(n*t(i+1)));
    a(i,12) = 1/(4*n^2*Rc)*(8*sin(n*t(i+1))-sin(2*n*t(i+1))-6*n*t(i+1));
    a(i,13) = 2*(3/(2*Rc)*(1-cos(n*t(i+1))));
    a(i,14) = 0;
    a(i,15) = 0;
    a(i,16) = 2*(3/(4*n*Rc)*(-5+4*cos(n*t(i+1))+cos(2*n*t(i+1))+4*n*t(i+1)*sin(n*t(i+1))));
    a(i,17) = 2*(3/(2*n*Rc)*(12*sin(n*t(i+1))+sin(2*n*t(i+1))-7*n*t(i+1)-7*n*t(i+1)*cos(n*t(i+1))));
    a(i,18) = 0;
    a(i,19) = 2*(3/(2*n*Rc)*(-sin(n*t(i+1))+n*t(i+1)));
    a(i,20) = 0;
    a(i,21) = 0;
    a(i,22) = 0;
    a(i,23) = 0;
    a(i,24) = 2*(1/(4*n*Rc)*(-3+4*cos(n*t(i+1))-cos(2*n*t(i+1))));
    a(i,25) = 2*(1/(2*n^2*Rc)*(-3+2*cos(n*t(i+1))+cos(2*n*t(i+1))+3*n*t(i+1)*sin(n*t(i+1))));
    a(i,26) = 0;
    a(i,27) = 0;
    
    %---Compute coefficients for QV z(t) equation:
    c(i,1) = 0;
    c(i,2) = 0;
    c(i,3) = cos(n*t(i+1));
    c(i,4) = 0;
    c(i,5) = 0;
    c(i,6) = (1/n *sin(n*t(i+1)));
    c(i,7) = 0;
    c(i,8) = 0;
    c(i,9) = 0;
    c(i,10) = 0;
    c(i,11) = 0;
    c(i,12) = 0;
    c(i,13) = 0;
    c(i,14) = 2*(3/(4*Rc)*(-3+2*cos(n*t(i+1))+cos(2*n*t(i+1))+4*n*t(i+1).*sin(n*t(i+1))));
    c(i,15) = 0;
    c(i,16) = 0;
    c(i,17) = 0;
    c(i,18) = 2*(3/(4*n*Rc)*(2*sin(n*t(i+1))+sin(2*n*t(i+1))-4*n*t(i+1).*cos(n*t(i+1))));
    c(i,19) = 0;
    c(i,20) = 0;
    c(i,21) = 0;
    c(i,22) = 2*(1/(4*n*Rc)*(2*sin(n*t(i+1))-sin(2*n*t(i+1))));
    c(i,23) = 2*(1/(2*n*Rc)*(-3+2*cos(n*t(i+1))+cos(2*n*t(i+1))+3*n*t(i+1).*sin(n*t(i+1))));
    c(i,24) = 0;
    c(i,25) = 0;
    c(i,26) = 2*(1/(4*n^2*Rc)*(3-4*cos(n*t(i+1))+cos(2*n*t(i+1))));
    c(i,27) = 2*(1/(2*n^2*Rc)*(sin(n*t(i+1))+sin(2*n*t(i+1))-3*n*t(i+1).*cos(n*t(i+1))));

    %---Compute measurement equation values:  (NOTE: The "i+1" index for
    %   the observation angles with noise, theta_N and phi_N, is used to
    %   skip the first element in the observation angle arrays which
    %   corresponds to an observation at t0.)
    A1(i,:) = sin(theta_N(i+1))*a(i,:)-cos(theta_N(i+1))*b(i,:);
    A2(i,:) = sin(phi_N(i+1))*a(i,:)-cos(phi_N(i+1))*c(i,:);
end

%---Form the A matrix from measurement equation values:
% A = [A1;A2(1:13,:)]; %Used to compare with Eigenvectors
A=[A1;A2];

%---Compute Singular Value Decomposition for the A matrix:
[U,S,V]=svd(A);

%% Solve for scale factor values
%---Preallocate arrays needed to solve for the scale factor:
h3 = zeros(6,27);
h4 = zeros(21,27);
alpha = zeros(27);

singvals = diag(S);  %---This value is currently NOT sent to output

%---Compute the 27x27 (or length(A(:,1)) x 27) matrix of possible scale factor (alpha) values:
for i=1:27
    h3(:,i) = [V(1,i); V(2,i); V(3,i); V(4,i); V(5,i); V(6,i)];
    h4(:,i) = [V(1,i)^2;V(2,i)^2;V(3,i)^2;V(4,i)^2;V(5,i)^2;V(6,i)^2;V(1,i)*V(2,i);V(1,i)*V(3,i);V(2,i)*V(3,i);V(1,i)*V(4,i);V(1,i)*V(5,i);V(1,i)*V(6,i);V(2,i)*V(4,i);V(2,i)*V(5,i);V(2,i)*V(6,i);V(3,i)*V(4,i);V(3,i)*V(5,i);V(3,i)*V(6,i);V(4,i)*V(5,i);V(4,i)*V(6,i);V(5,i)*V(6,i)];

     for j = 1:length(A(:,1))
        %---See equation 9 in AAS 14-293 (p.6)
        %---Rows of alpha correspond to measurements while columns
        %   correspond to singular vectors in V
        alpha(j,i) = -(A(j,1:6)*h3(:,i))/(A(j,7:27)*h4(:,i));   
    end
end
%% Loop through singular vectors and associated alpha values

%---Preallocate arrays:
    alphaSV = zeros(27,27);
    alphaSVmean = zeros(27);
    solvedIC_SV = zeros(6,27);
    x_vol_SV = zeros(num,3,27);
    theta_SV = zeros(num,27);
    phi_SV = zeros(num,27);
    resSVtheta = zeros((num-1),27);
    resSVphi = zeros((num-1),27);
    RMSerr_SV = zeros(1,27);
    alphaSV_length = zeros(1,27);

%% Process data without filtering alpha values
for SV_num = 1:27
    
    singvecs = V(:,SV_num);  %---SVD vector corresponding to singular vector (SV_num)
    
    %---Compute the mean of the unfiltered alpha values:
    [alphameantemp,alphaSVtemp] = alpha_filter_3D_mod(alpha(:,SV_num),singvecs(:,1),theta_N(1,1),phi_N(1,1));
    
    alphaSVmean(SV_num) = alphameantemp;  %---Create array containing mean alpha values
    alphaSV_length(SV_num) = length(alphaSVtemp);
    for icount = 1:length(alphaSVtemp)
        alphaSV(icount,SV_num) = alphaSVtemp(icount); %---Store alpha values for each singular vector
    end
    
    %---Compute solved ICs with filtered mean alpha value:
    solvedIC_SV(:,SV_num) = alphaSVmean(SV_num)*V(1:6,SV_num);
    
    %---Use QV relative motion equations to determine QV-predicted states at
    %   measurement times:
    x_vol_SV(:,:,SV_num) = vol_sol(t,n,Rc,solvedIC_SV(1,SV_num),solvedIC_SV(2,SV_num),solvedIC_SV(3,SV_num),solvedIC_SV(4,SV_num),solvedIC_SV(5,SV_num),solvedIC_SV(6,SV_num));
    
    %---Compute predicted observation angles using QV solution results at
    %   measurement times:
    theta_SV(:,SV_num) = atan2(x_vol_SV(:,1,SV_num),x_vol_SV(:,2,SV_num));
    phi_SV(:,SV_num)   = atan2(x_vol_SV(:,3,SV_num),x_vol_SV(:,2,SV_num));
    
    %---Compute the residuals of the observation angles found using the
    %   computed ICs found and the filtered alpha values for the specified
    %   singular vector (NOTE: Starting at the index value "2" skips the
    %   first value in the theta_N and phi_N arrays which actually corresponds
    %   to the initial state, not the first measurement.):
    resSVtheta(:,SV_num) = theta_N(2:end) - theta_SV(2:end,SV_num); % true_theta_noise - QV_theta
    resSVphi(:,SV_num)   = phi_N(2:end) - phi_SV(2:end,SV_num);     % true_phi_noise   - QV_phi
    
        
        
    %---Resolve observation angle residuals to the range -pi to pi:
    for i=1:(num-1)
        if resSVtheta(i,SV_num) > pi
            resSVtheta(i,SV_num) = resSVtheta(i,SV_num)-2*pi;
        elseif resSVtheta(i,SV_num) < -pi
            resSVtheta(i,SV_num) = resSVtheta(i,SV_num)+2*pi;
        end
        if resSVphi(i,SV_num) > pi
            resSVphi(i,SV_num) = resSVphi(i,SV_num)-2*pi;
        elseif resSVphi(i,SV_num) < -pi
            resSVphi(i,SV_num) = resSVphi(i,SV_num)+2*pi;
        end
    end
    
    %---Compute total RMS error (QV with mean alpha unfiltered solution compared to QV with IC) of
    %   angle residuals:
%     RMSerr_SV(1,SV_num) = sqrt((sum(resSVtheta(:,SV_num).^2,1)+sum(resSVphi(:,SV_num).^2,1))/(length(resSVtheta(:,SV_num))+length(resSVphi(:,SV_num))));
    RMSerr_SV(1,SV_num) = sqrt((sum(resSVtheta(:,SV_num).^2,1)+sum(resSVphi(:,SV_num).^2,1))/(length(resSVtheta(:,SV_num))+length(resSVphi(:,SV_num))));

end

%% Process data after filtering alpha values
    alphaSV_filtered = zeros(27,27);
    alphaSVmean_filtered = zeros(27);
    solvedIC_SV_filtered = zeros(6,27);
    x_vol_SV_filtered = zeros(num,3,27);
    theta_SV_filtered = zeros(num,27);
    phi_SV_filtered = zeros(num,27);
    resSVtheta_filtered = zeros((num-1),27);
    resSVphi_filtered = zeros((num-1),27);
    RMSerr_SV_filtered = zeros(1,27);
    alphaSV_length_filtered = zeros(1,27);
      
for SV_num = 1:27
    
    singvecs = V(:,SV_num);  %---SVD vector corresponding to singular vector (SV_num)
    
    %---Filter out alphas with incorrect sign based on observation angle values
    %   and eigenvector values at t0:
    [alphameantemp_filtered,alphaSVtemp_filtered] = alpha_filter_3D_dot1stMeas(alpha(:,SV_num),singvecs(:,1),theta_N(1,1),phi_N(1,1),unit_1stMeas_direction);

    alphaSVmean_filtered(SV_num) = alphameantemp_filtered;  %---Create array containing mean alpha values
    
    alphaSV_length_filtered(SV_num) = length(alphaSVtemp_filtered);
    
    for icount = 1:length(alphaSVtemp_filtered)
        alphaSV_filtered(icount,SV_num) = alphaSVtemp_filtered(icount); %---Store alpha values for each singular vector
    end
    
    %---Compute solved ICs with filtered mean alpha value:
    solvedIC_SV_filtered(:,SV_num) = alphaSVmean_filtered(SV_num)*V(1:6,SV_num);
   
    %---Use QV relative motion equations to determine QV-predicted states at
    %   measurement times:
    x_vol_SV_filtered(:,:,SV_num) = vol_sol(t,n,Rc,solvedIC_SV_filtered(1,SV_num),solvedIC_SV_filtered(2,SV_num),solvedIC_SV_filtered(3,SV_num),solvedIC_SV_filtered(4,SV_num),solvedIC_SV_filtered(5,SV_num),solvedIC_SV_filtered(6,SV_num));
    
    %---Compute predicted observation angles using QV solution results at
    %   measurement times:
    theta_SV_filtered(:,SV_num) = atan2(x_vol_SV_filtered(:,1,SV_num),x_vol_SV_filtered(:,2,SV_num));
    phi_SV_filtered(:,SV_num)   = atan2(x_vol_SV_filtered(:,3,SV_num),x_vol_SV_filtered(:,2,SV_num));
  
            
    %---Compute the residuals of the observation angles found using the
    %   computed ICs found and the filtered alpha values for the specified
    %   singular vector (NOTE: Starting at the index value "2" skips the
    %   first value in the theta_N and phi_N arrays which actually corresponds
    %   to the initial state, not the first measurement.):
    resSVtheta_filtered(:,SV_num) = theta_N(2:end) - theta_SV_filtered(2:end,SV_num); % true_theta_noise - QV_theta
    resSVphi_filtered(:,SV_num)   = phi_N(2:end) - phi_SV_filtered(2:end,SV_num);     % true_phi_noise   - QV_phi
    
    %---Resolve observation angle residuals to the range -pi to pi:
    for i=1:(num-1)
        if resSVtheta_filtered(i,SV_num) > pi
            resSVtheta_filtered(i,SV_num) = resSVtheta_filtered(i,SV_num)-2*pi;
        elseif resSVtheta_filtered(i,SV_num) < -pi
            resSVtheta_filtered(i,SV_num) = resSVtheta_filtered(i,SV_num)+2*pi;
        end
        if resSVphi_filtered(i,SV_num) > pi
            resSVphi_filtered(i,SV_num) = resSVphi_filtered(i,SV_num)-2*pi;
        elseif resSVphi_filtered(i,SV_num) < -pi
            resSVphi_filtered(i,SV_num) = resSVphi_filtered(i,SV_num)+2*pi;
        end
    end
    
    %---Compute total RMS error (QV mean alpha filtered solution compared to QV with IC) of
    %   angle residuals:
    RMSerr_SV_filtered(1,SV_num) = sqrt((sum(resSVtheta_filtered(:,SV_num).^2,1)+sum(resSVphi_filtered(:,SV_num).^2,1))/(length(resSVtheta_filtered(:,SV_num))+length(resSVphi_filtered(:,SV_num))));

end

%---Determine which singular vector yielded the minimum RMS error using the
%     mean alpha scaling factor:
[~,Imin] = min(RMSerr_SV);
[~,Imin_filtered] = min(RMSerr_SV_filtered);
% 
%---NOTE:  "Imin" is the index for the singular vector associated with the
%     unfiltered mean alpha value that produced the minimum RMS error.
%     This value is used for further analysis in the upcoming calculations.
%     "Imin_filtered" is the index associated with the filtered mean alpha
%     value that minimized the RMS error.

%--------------------------------------------------------------------------
%% Compute residuals and errors for QV-predicted measurements vs 'true' 
%  RK or QV propagated measurement values (NOTE: Only for simulated
%  measurements). In this case, the true measurement values are based on
%  either the RK or QV model with ICs

if data_type == 1
    
    %---Use QV relative motion equations and true ICs to determine QV-predicted
    %   states at measurement times:
    x_vol = vol_sol(t,n,Rc,x0,y0,z0,xdot0,ydot0,zdot0);
    
    %---Compute predicted observation angles using QV solution method and true
    %   ICs at desired measurement times:
    theta_qv = atan2(x_vol(:,1),x_vol(:,2));
    phi_qv = atan2(x_vol(:,3),x_vol(:,2));
    
    %---Compute the residuals of the observation angles found using the
    %   true ICs and the QV relative motion solution and the true measurement
    %   angles WITHOUT noise (NOTE: Starting at the index value "2" skips the
    %   first value in the theta and phi arrays - the first value actually
    %   corresponds to the initial state, not the first measurement.):
    restheta_SIMvsQV = theta(2:end) - theta_qv(2:end);
    resphi_SIMvsQV = phi(2:end) - phi_qv(2:end);
    
    %---Resolve observation angle residuals to the range -pi to pi:
    for i=1:(num-1)
        if restheta_SIMvsQV(i) > pi
            restheta_SIMvsQV(i) = restheta_SIMvsQV(i)-2*pi;
        elseif restheta_SIMvsQV(i) < -pi
            restheta_SIMvsQV(i) = restheta_SIMvsQV(i)+2*pi;
        end
        if resphi_SIMvsQV(i) > pi
            resphi_SIMvsQV(i) = resphi_SIMvsQV(i)-2*pi;
        elseif resphi_SIMvsQV(i) < -pi
            resphi_SIMvsQV(i) = resphi_SIMvsQV(i)+2*pi;
        end
    end
    
    %---Compute total RMS error of residuals: (If no process noise is being used for observation generation, 
%     this RMS result should be zero, as it consists of a comparison between 
%     the same QV propagated trajectory with the selected ICs)
    RMSerr_SIMvsQV = sqrt((sum(restheta_SIMvsQV.^2,1)+sum(resphi_SIMvsQV.^2,1))/(length(restheta_SIMvsQV)+length(resphi_SIMvsQV)));

end

%--------------------------------------------------------------------------
%% Compute other performance metrics applicable to all data types

%---Compute the relative range between the Chief and Deputy using the
%   initial states that result from processing the simulated measurements
%   with noise using the QV solution method for all observation times:
% range_SV = sqrt(x_vol_SV(2:end,1,Imin).^2+x_vol_SV(2:end,2,Imin).^2+x_vol_SV(2:end,3,Imin).^2);
range_SV = sqrt(x_vol_SV(:,1,Imin).^2+x_vol_SV(:,2,Imin).^2+x_vol_SV(:,3,Imin).^2);
range_SV_filtered = sqrt(x_vol_SV_filtered(:,1,Imin_filtered).^2+x_vol_SV_filtered(:,2,Imin_filtered).^2+x_vol_SV_filtered(:,3,Imin_filtered).^2);

%--------------------------------------------------------------------------
%% Compute metrics for simulated data only

if data_type == 1
    
    %---Compute ratio of the magnitudes of the 'true' IC state vector to the IC
    %   state vector determined using the QV solution method and the mean value
    %   of alpha with and without filtering of alpha values with the wrong sign:
    statemagratio_SV = sqrt(sum(IC.^2))/sqrt(sum(solvedIC_SV(1:6,Imin).^2));
    statemagratio_SV_filtered = sqrt(sum(IC.^2))/sqrt(sum(solvedIC_SV_filtered(1:6,Imin_filtered).^2));
    
    %---Compute a measure of how well aligned the 'true' IC state vector is
    %   compared to the IC state vector obtained with the QV solution method.
    %   This is essentially the cosine of the angle between the vectors and has
    %   a max value of 1 if they are perfectly aligned:
    statedir_ratio_SV = dot(IC,solvedIC_SV(1:6,Imin))/(norm(IC)*norm(solvedIC_SV(1:6,Imin)));
    statedir_ratio_SV_filtered = dot(IC,solvedIC_SV_filtered(1:6,Imin_filtered))/(norm(IC)*norm(solvedIC_SV_filtered(1:6,Imin_filtered)));

    %---Compute the relative range between the Chief and Deputy using the
    %   'true' states for all observation times:
    range_IC = sqrt(xobs(:).^2+yobs(:).^2+zobs(:).^2);
    
    %---Compute the relative range between the Chief and Deputy using the
    %   states predicted using the QV relative motion equations and the 'true'
    %   initial state for all observation times:
    range_QV = sqrt(x_vol(:,1).^2+x_vol(:,2).^2+x_vol(:,3).^2);
    
    %---Compute the range residuals for the solutions that involve QV
    %   solutions for all observation times:
    rangeresSV = range_IC - range_SV;
    rangeresSV_filtered = range_IC - range_SV_filtered;
    rangeresQV = range_IC - range_QV;

    %---Compute the RMS error of the range residuals:
    range_RMSerr_SV = sqrt(sum(rangeresSV.^2,1)/length(rangeresSV));
    range_RMSerr_SV_filtered = sqrt(sum(rangeresSV_filtered.^2,1)/length(rangeresSV_filtered));
    range_RMSerr_SIMvsQV = sqrt(sum(rangeresQV.^2,1)/length(rangeresQV));

    %---Compute the ratio of the true range to the range obtained by processing
    %   the simulated measurements and to the range obtained using the position
    %   predicted by the QV relative motion equations:
    rangescaleSV = range_IC./range_SV;
    rangescaleSV_filtered = range_IC./range_SV_filtered;
    rangescaleQV = range_IC./range_QV;
    
    %---Compute the RMS of the range ratios:
    range_RMSscale_SV = sqrt(sum(rangescaleSV.^2,1)/length(rangescaleSV));
    range_RMSscale_SV_filtered = sqrt(sum(rangescaleSV_filtered.^2,1)/length(rangescaleSV_filtered));
    range_RMSscale_QV = sqrt(sum(rangescaleQV.^2,1)/length(rangescaleQV));

end

%--------------------------------------------------------------------------
%% Perform an unfiltered line search to determine the value for alpha that minimizes
%    the total RMS error of the observation angle residuals

%---Set min and max values of alpha based on previously computed alphas to 
%   bound the line search

%   NOTE:  The line search is performed for the alphas associated with
%   each of the the 27 singular vectors.

%---Create a truncated array with "empty" storage locations removed
%   (The array alphaSV was sized to contain values for 27 measurements.
%   Some of the values were removed when the alpha values were filtered
%   leaving empty slots in the array which were filled with zeros.  The
%   zero value causes problems when computing the high and low alpha values
%   for the line search so the alpha_temp array is sized correctly to avoid
%   the problems.

%---Preallocate arrays:
minRMS_search = zeros(1,27);
alpha_search = zeros(1,27);
X0_minRMS = zeros(6,27);
statemagratio_search = zeros(1,27);
statedir_ratio_search = zeros(1,27);
x_vol_line_search = zeros(num,3,27);
theta_search = zeros(num,27);
phi_search = zeros(num,27);
range_search = zeros(num,27);
rangeres_search = zeros(num,27);
range_RMSerr_search = zeros(1,27);
rangescale_search = zeros(num,27);
range_RMSscale_search = zeros(1,27);

%---Loop through all singular vectors:
for SV_num = 1:27
    
    alpha_temp = zeros(1,alphaSV_length(SV_num));
    
    for jcount = 1:alphaSV_length(SV_num)
        alpha_temp(jcount) = alphaSV(jcount,SV_num);
    end
    
    %---Set high/low range for alphas based on values in alpha array:
    if alphaSV(1,SV_num) > 0
        high = 1.5*max(alpha_temp);  % QUESTION: Why is a factor of "1.5" used?
        low = 0.5*min(alpha_temp);   % QUESTION: Why is a factor of "0.5) used?
    else
        high = 0.5*max(alpha_temp);
        low = 1.5*min(alpha_temp);
    end
    
    %---Create a linearly spaced vector of alpha values for the line search:
    Bnew = linspace(low,high,line_search_pts);
    
    %---Preallocate variables:
    X02 = zeros(6,length(Bnew));
%     differ = zeros(6,length(Bnew));
    RMSerr2 = zeros(1,length(Bnew));
    
    %---Loop through possible alpha values between low and high values:
    for i = 1:length(Bnew)
        
        %---Compute scaled state using current alpha value and singular
        %     vector:
        %ORIGINAL    X02(:,i) = Bnew(i)*V(1:6,meq_num);
        X02(:,i) = Bnew(i)*V(1:6,SV_num);
        
        %---Compute difference between scaled state and true initial condition
        %   state:
%         differ(:,i) = X02(:,i)-IC;
        
        %---Use QV relative motion equations to determine QV-predicted states at
        %   measurement times using the state that is scaled with the current
        %   value of alpha:
        x_vol_search = vol_sol(t,n,Rc,X02(1,i),X02(2,i),X02(3,i),X02(4,i),X02(5,i),X02(6,i));
        
        %---Compute the observation angles that correspond to the QV-predicted
        %   states:
        theta2 = atan2(x_vol_search(:,1),x_vol_search(:,2));
        phi2   = atan2(x_vol_search(:,3),x_vol_search(:,2));
        
        %---Compute the residuals of the observation angles found using the
        %   QV relative motion solution and the true measurement angles with
        %   noise (NOTE: Starting at the index value "2" skips the first value
        %   in the theta_N and phi_N arrays which actually corresponds
        %   to the initial state, not the first measurement.):
        restheta = theta_N(2:end) - theta2(2:end); %true_theta_noise - QV_theta
        resphi = phi_N(2:end) - phi2(2:end);       %true_phi_noise   - QV_phi
        
        %---Resolve observation angle residuals to the range -pi to pi:
        for j=1:(num-1)
            if restheta(j) > pi
                restheta(j) = restheta(j)-2*pi;
            elseif restheta(j) < -pi
                restheta(j) = restheta(j)+2*pi;
            end
            if resphi(j) > pi
                resphi(j) = resphi(j)-2*pi;
            elseif resphi(j) < -pi
                resphi(j) = resphi(j)+2*pi;
            end
        end
        
        %---Compute the total RMS error of the observation angle residuals:
        RMSerr2(i) = sqrt((sum(restheta.^2,1)+sum(resphi.^2,1))/(length(restheta)+length(resphi)));
        
    end
    
    %---Determine which alpha in the search range minimized the RMS error:
    [minRMS_search(SV_num),I] = min(RMSerr2);
    
    %---Set the desired alpha value based on the value that minimized the RMS
    %   error:
    alpha_search(SV_num) = Bnew(I);
    
    %---Set the scaled initial state based on the alpha that minimized the RMS
    %   error:
    X0_minRMS(:,SV_num) = X02(:,I);
    
    %---Compute metrics for simulated data only:
    if data_type == 1
        %---Compute ratio of the magnitudes of the 'true' IC state vector to the IC
        %   state vector determined using the QV solution method and the simple
        %   line search for alpha:
        statemagratio_search(SV_num) = sqrt(sum(IC.^2))/sqrt(sum(X0_minRMS(1:6,SV_num).^2));
        
        %---Compute a measure of how well aligned the 'true' IC state vector is
        %   compared to the IC state vector obtained with the QV solution method
        %   and the simple line search for alpha.
        %   This is essentially the cosine of the angle between the vectors and has
        %   a max value of 1 if they are perfectly aligned:
        statedir_ratio_search(SV_num) = dot(IC,X0_minRMS(1:6,SV_num))/(norm(IC)*norm(X0_minRMS(1:6,SV_num)));
    end
    
    %---Use QV relative motion equations to determine QV-predicted states at
    %   measurement times using the state that is scaled with the alpha
    %   value that minimized the observation angle RMS error:
    x_vol_line_search(:,:,SV_num) = vol_sol(t,n,Rc,X02(1,I),X02(2,I),X02(3,I),X02(4,I),X02(5,I),X02(6,I));
    
    %---Compute the observation angles that correspond to the QV-predicted
    %   states and selected alpha value:
    theta_search(:,SV_num) = atan2(x_vol_line_search(:,1,SV_num),x_vol_line_search(:,2,SV_num));
    phi_search(:,SV_num) = atan2(x_vol_line_search(:,3,SV_num),x_vol_line_search(:,2,SV_num));
    
    %---Compute the relative range between the Chief and Deputy using the
    %   states predicted using the QV relative motion equations and the
    %   selected alpha value:
    range_search(:,SV_num) = sqrt(x_vol_line_search(:,1,SV_num).^2+x_vol_line_search(:,2,SV_num).^2+x_vol_line_search(:,3,SV_num).^2);
    
    if data_type == 1
        %---Compute the range residuals:
        rangeres_search(:,SV_num) = range_IC - range_search(:,SV_num);
        
        %---Compute the RMS error of the range residuals:
        range_RMSerr_search(SV_num) = sqrt(sum(rangeres_search(:,SV_num).^2,1)/length(rangeres_search(:,SV_num)));
        
        %---Compute the ratio of the true range to the range obtained using the
        %   position predicted by the QV relative motion equations and the selected
        %   alpha value:
        rangescale_search(:,SV_num) = range_IC./range_search(:,SV_num);
        
        %---Compute the RMS of the range ratios:
        range_RMSscale_search(SV_num) = sqrt(sum(rangescale_search(:,SV_num).^2,1)/length(rangescale_search(:,SV_num)));
    end
end

%---Determine which singular vector yielded the lowest total RMS error of
%     the observation angle residuals based on a line search for alpha:
[~,Imin_search] = min(minRMS_search);


%%
%% Perform a filtered line search to determine the value for alpha that minimizes
%    the total RMS error of the observation angle residuals

%---Set min and max values of alpha based on previously computed alphas to 
%   bound the line search

%   NOTE:  The line search is performed for the alphas associated with
%   each of the the 27 singular vectors.

%---Create a truncated array with "empty" storage locations removed
%   (The array alphaSV was sized to contain values for 27 measurements.
%   Some of the values were removed when the alpha values were filtered
%   leaving empty slots in the array which were filled with zeros.  The
%   zero value causes problems when computing the high and low alpha values
%   for the line search so the alpha_temp array is sized correctly to avoid
%   the problems.

%---Preallocate arrays:
minRMS_search_f = zeros(1,27);
alpha_search_f = zeros(1,27);
X0_minRMS_f = zeros(6,27);
statemagratio_search_f = zeros(1,27);
statedir_ratio_search_f = zeros(1,27);
x_vol_line_search_f = zeros(num,3,27);
theta_search_f = zeros(num,27);
phi_search_f = zeros(num,27);
range_search_f = zeros(num,27);
rangeres_search_f = zeros(num,27);
range_RMSerr_search_f = zeros(1,27);
rangescale_search_f = zeros(num,27);
range_RMSscale_search_f = zeros(1,27);

for SV_num = 1:27
    
    alpha_temp_filtered_f = zeros(1, alphaSV_length_filtered(SV_num));
    
    for jcount = 1: alphaSV_length_filtered(SV_num)
        alpha_temp_filtered_f(jcount) = alphaSV_filtered(jcount,SV_num);
    end
    
    %---Set high/low range for alphas based on values in alpha array:
    if alphaSV_filtered(1,SV_num) > 0
        high = 1.5*max(alpha_temp_filtered_f);  % QUESTION: Why is a factor of "1.5" used?
        low = 0.5*min(alpha_temp_filtered_f);   % QUESTION: Why is a factor of "0.5) used?
    else
        high = 0.5*max(alpha_temp_filtered_f);
        low = 1.5*min(alpha_temp_filtered_f);
    end
    
    %---Create a linearly spaced vector of alpha values for the line search:
    Bnew_f = linspace(low,high,line_search_pts);
    
    %---Preallocate variables:
    X02_f = zeros(6,length(Bnew_f));
%     differ = zeros(6,length(Bnew));
    RMSerr2_f = zeros(1,length(Bnew_f));
    
    %---Loop through possible alpha values between low and high values:
    for i = 1:length(Bnew_f)
        
        %---Compute scaled state using current alpha value and singular
        %     vector:
        %ORIGINAL    X02(:,i) = Bnew(i)*V(1:6,meq_num);
        X02_f(:,i) = Bnew_f(i)*V(1:6,SV_num);
        
        %---Compute difference between scaled state and true initial condition
        %   state:
%         differ(:,i) = X02(:,i)-IC;
        
        %---Use QV relative motion equations to determine QV-predicted states at
        %   measurement times using the state that is scaled with the current
        %   value of alpha:
        x_vol_search_f = vol_sol(t,n,Rc,X02_f(1,i),X02_f(2,i),X02_f(3,i),X02_f(4,i),X02_f(5,i),X02_f(6,i));
        
        %---Compute the observation angles that correspond to the QV-predicted
        %   states:
        theta2_f = atan2(x_vol_search_f(:,1),x_vol_search_f(:,2));
        phi2_f   = atan2(x_vol_search_f(:,3),x_vol_search_f(:,2));
        
        %---Compute the residuals of the observation angles found using the
        %   QV relative motion solution and the true measurement angles with
        %   noise (NOTE: Starting at the index value "2" skips the first value
        %   in the theta_N and phi_N arrays which actually corresponds
        %   to the initial state, not the first measurement.):
        restheta_f = theta_N(2:end) - theta2_f(2:end); %true_theta_noise - QV_theta
        resphi_f = phi_N(2:end) - phi2_f(2:end);       %true_phi_noise   - QV_phi
        
        %---Resolve observation angle residuals to the range -pi to pi:
        for j=1:(num-1)
            if restheta_f(j) > pi
                restheta_f(j) = restheta_f(j)-2*pi;
            elseif restheta_f(j) < -pi
                restheta_f(j) = restheta_f(j)+2*pi;
            end
            if resphi_f(j) > pi
                resphi_f(j) = resphi_f(j)-2*pi;
            elseif resphi_f(j) < -pi
                resphi_f(j) = resphi_f(j)+2*pi;
            end
        end
        
        %---Compute the total RMS error of the observation angle residuals:
        RMSerr2_f(i) = sqrt((sum(restheta_f.^2,1)+sum(resphi_f.^2,1))/(length(restheta_f)+length(resphi_f)));
        
    end
    
    %---Determine which alpha in the search range minimized the RMS error:
    [minRMS_search_f(SV_num),I] = min(RMSerr2_f);
    
    %---Set the desired alpha value based on the value that minimized the RMS
    %   error:
    alpha_search_f(SV_num) = Bnew_f(I);
    
    %---Set the scaled initial state based on the alpha that minimized the RMS
    %   error:
    X0_minRMS_f(:,SV_num) = X02_f(:,I);
    
    %---Compute metrics for simulated data only:
    if data_type == 1
        %---Compute ratio of the magnitudes of the 'true' IC state vector to the IC
        %   state vector determined using the QV solution method and the simple
        %   line search for alpha:
        statemagratio_search_f(SV_num) = sqrt(sum(IC.^2))/sqrt(sum(X0_minRMS_f(1:6,SV_num).^2));
        
        %---Compute a measure of how well aligned the 'true' IC state vector is
        %   compared to the IC state vector obtained with the QV solution method
        %   and the simple line search for alpha.
        %   This is essentially the cosine of the angle between the vectors and has
        %   a max value of 1 if they are perfectly aligned:
        statedir_ratio_search_f(SV_num) = dot(IC,X0_minRMS_f(1:6,SV_num))/(norm(IC)*norm(X0_minRMS_f(1:6,SV_num)));
    end
    
    %---Use QV relative motion equations to determine QV-predicted states at
    %   measurement times using the state that is scaled with the alpha
    %   value that minimized the observation angle RMS error:
    x_vol_line_search_f(:,:,SV_num) = vol_sol(t,n,Rc,X02_f(1,I),X02_f(2,I),X02_f(3,I),X02_f(4,I),X02_f(5,I),X02_f(6,I));
    
    %---Compute the observation angles that correspond to the QV-predicted
    %   states and selected alpha value:
    theta_search_f(:,SV_num) = atan2(x_vol_line_search_f(:,1,SV_num),x_vol_line_search_f(:,2,SV_num));
    phi_search_f(:,SV_num) = atan2(x_vol_line_search_f(:,3,SV_num),x_vol_line_search_f(:,2,SV_num));
    
    %---Compute the relative range between the Chief and Deputy using the
    %   states predicted using the QV relative motion equations and the
    %   selected alpha value:
    range_search_f(:,SV_num) = sqrt(x_vol_line_search_f(:,1,SV_num).^2+x_vol_line_search_f(:,2,SV_num).^2+x_vol_line_search_f(:,3,SV_num).^2);
    
    if data_type == 1
        %---Compute the range residuals:
        rangeres_search_f(:,SV_num) = range_IC - range_search_f(:,SV_num);
        
        %---Compute the RMS error of the range residuals:
        range_RMSerr_search_f(SV_num) = sqrt(sum(rangeres_search_f(:,SV_num).^2,1)/length(rangeres_search_f(:,SV_num)));
        
        %---Compute the ratio of the true range to the range obtained using the
        %   position predicted by the QV relative motion equations and the selected
        %   alpha value:
        rangescale_search_f(:,SV_num) = range_IC./range_search_f(:,SV_num);
        
        %---Compute the RMS of the range ratios:
        range_RMSscale_search_f(SV_num) = sqrt(sum(rangescale_search_f(:,SV_num).^2,1)/length(rangescale_search_f(:,SV_num)));
    end
end

%---Determine which singular vector yielded the lowest total RMS error of
%     the observation angle residuals based on a line search for alpha:
[~,Imin_search_f] = min(minRMS_search_f);


%--------------------------------------------------------------------------
%% Compute Other Performance Metrics for Output

%---Compute ROEs using IC values obtained by processing simulated 
%   measurements and mean value of unfiltered scaling factor (alpha):
a_SV = 2*sqrt((solvedIC_SV(4,Imin)/n)^2+(3*solvedIC_SV(1,Imin)+2*solvedIC_SV(5,Imin)/n)^2);
x_SV = 4*solvedIC_SV(1,Imin)+2*solvedIC_SV(5,Imin)/n;
y_SV = solvedIC_SV(2,Imin)-2*solvedIC_SV(4,Imin)/n;
Beta_SV = atan2(solvedIC_SV(4,Imin),3*n*solvedIC_SV(1,Imin)+2*solvedIC_SV(5,Imin));
z_max_SV = sqrt((solvedIC_SV(6,Imin)/n)^2+solvedIC_SV(3,Imin)^2);
gamma_SV = atan2(n*solvedIC_SV(3,Imin),solvedIC_SV(6,Imin))-Beta_SV;

%---Compute ROEs using IC values obtained by processing simulated 
%   measurements and mean value of filtered scaling factor (alpha):
a_SV_filtered = 2*sqrt((solvedIC_SV_filtered(4,Imin_filtered)/n)^2+(3*solvedIC_SV_filtered(1,Imin_filtered)+2*solvedIC_SV_filtered(5,Imin_filtered)/n)^2);
x_SV_filtered = 4*solvedIC_SV_filtered(1,Imin_filtered)+2*solvedIC_SV_filtered(5,Imin_filtered)/n;
y_SV_filtered = solvedIC_SV_filtered(2,Imin_filtered)-2*solvedIC_SV_filtered(4,Imin_filtered)/n;
Beta_SV_filtered = atan2(solvedIC_SV_filtered(4,Imin_filtered),3*n*solvedIC_SV_filtered(1,Imin_filtered)+2*solvedIC_SV_filtered(5,Imin_filtered));
z_max_SV_filtered = sqrt((solvedIC_SV_filtered(6,Imin_filtered)/n)^2+solvedIC_SV_filtered(3,Imin_filtered)^2);
gamma_SV_filtered = atan2(n*solvedIC_SV_filtered(3,Imin_filtered),solvedIC_SV_filtered(6,Imin_filtered))-Beta_SV_filtered;

%---Compute ROEs using IC values obtained by processing simulated 
%   measurements and scaling factor (alpha) found using simple unfiltered line search:
a_search = 2*sqrt((X0_minRMS(4,Imin_search)/n)^2+(3*X0_minRMS(1,Imin_search)+2*X0_minRMS(5,Imin_search)/n)^2);
x_search = 4*X0_minRMS(1,Imin_search)+2*X0_minRMS(5,Imin_search)/n;
y_search = X0_minRMS(2,Imin_search)-2*X0_minRMS(4,Imin_search)/n;
Beta_search = atan2(X0_minRMS(4,Imin_search),3*n*X0_minRMS(1,Imin_search)+2*X0_minRMS(5,Imin_search));
z_max_search = sqrt((X0_minRMS(6,Imin_search)/n)^2+X0_minRMS(3,Imin_search)^2);
gamma_search = atan2(n*X0_minRMS(3,Imin_search),X0_minRMS(6,Imin_search))-Beta_search;

%---Compute ROEs using IC values obtained by processing simulated 
%   measurements and scaling factor (alpha) found using a simple filtered line search:
a_search_f = 2*sqrt((X0_minRMS_f(4,Imin_search_f)/n)^2+(3*X0_minRMS_f(1,Imin_search_f)+2*X0_minRMS_f(5,Imin_search_f)/n)^2);
x_search_f = 4*X0_minRMS_f(1,Imin_search_f)+2*X0_minRMS_f(5,Imin_search_f)/n;
y_search_f = X0_minRMS_f(2,Imin_search_f)-2*X0_minRMS_f(4,Imin_search_f)/n;
Beta_search_f = atan2(X0_minRMS_f(4,Imin_search_f),3*n*X0_minRMS_f(1,Imin_search_f)+2*X0_minRMS_f(5,Imin_search_f));
z_max_search_f = sqrt((X0_minRMS_f(6,Imin_search_f)/n)^2+X0_minRMS_f(3,Imin_search_f)^2);
gamma_search_f = atan2(n*X0_minRMS_f(3,Imin_search_f),X0_minRMS_f(6,Imin_search_f))-Beta_search_f;

%---Form arrays of ROEs obtained from true states and from states 
%   determined by processing simulated measurements with mean value of
%   scaling factor (alpha) and alpha value found by simple line search:
ROE_search = [a_search;x_search;y_search;Beta_search;z_max_search;gamma_search];

%---Compute metrics for simulated data only:
if data_type == 1
    %---Compute ROEs using "true" IC values:
    a_IC = 2*sqrt((IC(4)/n)^2+(3*IC(1)+2*IC(5)/n)^2);
    x_IC = 4*IC(1)+2*IC(5)/n;
    y_IC = IC(2)-2*IC(4)/n;
    Beta_IC = atan2(IC(4),3*n*IC(1)+2*IC(5));
    z_max_IC = sqrt((IC(6)/n)^2+IC(3)^2);
    gamma_IC = atan2(n*IC(3),IC(6))-Beta_IC;
    
    %---Form arrays of ROEs obtained from true states and from states
    %   determined by processing simulated measurements with mean value of
    %   scaling factor (alpha) and alpha value found by simple line search:
    ROE_IC = [a_IC;x_IC;y_IC;Beta_IC;z_max_IC;gamma_IC];

    %---Compute ratio of the magnitudes of the 'true' ROEs to the ROEs
    %   determined using the QV solution method and processing the simulated
    %   measurements along with the scaling factor found using the simple line
    %   search:
    ROEmagratio_search = sqrt(sum(ROE_IC.^2))/sqrt(sum(ROE_search.^2));

    %---Compute a measure of how well aligned the true ROE vector is
    %   compared to the ROE vector obtained with the QV solution method
    %   and the simple line search for alpha.
    %   This is essentially the cosine of the angle between the vectors and has
    %   a max value of 1 if they are perfectly aligned:
    ROEdir_ratio_search = dot(ROE_IC,ROE_search)/(norm(ROE_IC)*norm(ROE_search));

end

%RMSE of position for the true model vs a QV propagated trajectory using each of the solved ICs
%Mean unfiltered
RSS_x= (x_vol(:,1)-x_vol_SV(:,1, Imin));
RSS_y=(x_vol(:,2)-x_vol_SV(:,2, Imin));
RSS_z=(x_vol(:,3)-x_vol_SV(:,3,Imin));

Position_RSS_Mean_Unfiltered=sqrt(sum((RSS_x).^2+(RSS_y).^2+(RSS_z).^2));

% Mean filtered
RSS_xf= (x_vol(:,1)-x_vol_SV_filtered(:,1,Imin_filtered));
RSS_yf=(x_vol(:,2)-x_vol_SV_filtered(:,2,Imin_filtered));
RSS_zf=(x_vol(:,3)-x_vol_SV_filtered(:,3,Imin_filtered));

Position_RSS_Mean_Filtered=sqrt(sum((RSS_xf).^2+(RSS_yf).^2+(RSS_zf).^2));

% %Line search unfiltered
RSS_x_line= (x_vol(:,1)-x_vol_line_search(:,1,Imin_search));
RSS_y_line=(x_vol(:,2)-x_vol_line_search(:,2,Imin_search));
RSS_z_line=(x_vol(:,3)-x_vol_line_search(:,3,Imin_search));

Position_RSS_Line_Unfiltered=sqrt(sum((RSS_x_line).^2+(RSS_y_line).^2+(RSS_z_line).^2));

% %Line search filtered
RSS_x_line_f= (x_vol(:,1)-x_vol_line_search_f(:,1,Imin_search_f));
RSS_y_line_f=(x_vol(:,2)-x_vol_line_search_f(:,2,Imin_search_f));
RSS_z_line_f=(x_vol(:,3)-x_vol_line_search_f(:,3,Imin_search_f));

Position_RSS_Line_Filtered=sqrt(sum((RSS_x_line_f).^2+(RSS_y_line_f).^2+(RSS_z_line_f).^2));

%--------------------------------------------------------------------------
%% Write output to screen and output files

%---Open output file:
fid = fopen('IOD_3D_Output_v11.txt','w');

%--------------------------------------------------------------------------
%---Output for all data types:
fprintf ('Best Singular Vector Number (unfiltered):       %e\n', Imin);
fprintf (fid, 'Best Singular Vector Number (unfiltered):  %e\n', Imin);
fprintf ('Best Singular Vector Number (filtered):      %e\n', Imin_filtered);
fprintf (fid, 'Best Singular Vector Number (filtered): %e\n', Imin_filtered);
fprintf ('Best Singular Vector Number (unfiltered line search):      %e\n', Imin_search);
fprintf (fid, 'Best Singular Vector Number (unfiltered line search): %e\n', Imin_search);
fprintf ('Best Singular Vector Number (filtered line search):      %e\n\n', Imin_search_f);
fprintf (fid, 'Best Singular Vector Number (filtered line search): %e\n\n', Imin_search_f);

fprintf ('alphaSVmean (unfiltered):  %e\n', alphaSVmean(Imin));
fprintf ('alphaSVmean (filtered):    %e\n', alphaSVmean_filtered(Imin_filtered));
fprintf ('alpha_search (unfiltered line search): %e\n', alpha_search(Imin_search));
fprintf ('alpha_search (filtered line search):   %e\n\n', alpha_search_f(Imin_search_f));

fprintf (fid, 'alphaSVmean (unfiltered):      %e\n', alphaSVmean(Imin));
fprintf (fid, 'alphaSVmean (filtered):        %e\n', alphaSVmean_filtered(Imin_filtered));
fprintf (fid, 'alpha_search (unfiltered line search):   %e\n', alpha_search(Imin_search));
fprintf (fid, 'alpha_search (filtered line search):     %e\n\n', alpha_search_f(Imin_search_f));

fprintf('Deputy ECI x Position at t0:   %e\n', ECI_x0(1));
fprintf(fid,'Deputy ECI x Position at t0:   %e\n', ECI_x0(1));
fprintf('Deputy ECI y Position at t0:   %e\n', ECI_x0(2));
fprintf(fid,'Deputy ECI y Position at t0:   %e\n', ECI_x0(2));
fprintf('Deputy ECI z Position at t0:   %e\n', ECI_x0(3));
fprintf(fid,'Deputy ECI z Position at t0:   %e\n', ECI_x0(3));

fprintf('Deputy ECI x Velocity at t0:    %e\n', ECI_v0(1));
fprintf(fid, 'Deputy ECI x Velocity at t0:   %e\n', ECI_v0(1));
fprintf('Deputy ECI y Velocity at t0:    %e\n', ECI_v0(2));
fprintf(fid, 'Deputy ECI y Velocity at t0:   %e\n', ECI_v0(2));
fprintf('Deputy ECI z Velocity at t0:    %e\n', ECI_v0(3));
fprintf(fid, 'Deputy ECI z Velocity at t0:   %e\n', ECI_v0(3));

%--------------------------------------------------------------------------
%---Output for simulated data only:
if data_type == 1
    
    fprintf (fid, 'Method:  %s\n\n', method);
    
    disp(' ');
    disp('                     Initial Condition ROEs');
    disp('(NOTE:  Values are for the singular vector that yielded the minimum');
    disp('        total residual RMS error computed using the specified method to determine alpha.)');
    disp(  ' Source  :  True ICs         Line Search         Line Search         Mean Alpha         Mean Alpha');
    disp('                             (unfiltered)         (filtered)         (unfiltered)        (filtered)');
    fprintf ('ae:     %e        %e        %e       %e        %e\n', a_IC, a_search, a_search_f, a_SV, a_SV_filtered);
    fprintf ('xd:     %e        %e        %e       %e        %e\n', x_IC, x_search, x_search_f, x_SV, x_SV_filtered);
    fprintf ('yd:     %e        %e        %e       %e        %e\n', y_IC, y_search, y_search_f, y_SV, y_SV_filtered);
    fprintf ('Beta:   %e       %e       %e      %e       %e\n', Beta_IC, Beta_search, Beta_search_f, Beta_SV, Beta_SV_filtered);
    fprintf ('z_max:  %e        %e        %e       %e        %e\n', z_max_IC, z_max_search, z_max_search_f, z_max_SV, z_max_SV_filtered);
    fprintf ('gamma:  %e        %e        %e       %e        %e\n', gamma_IC, gamma_search, gamma_search_f, gamma_SV, gamma_SV_filtered);
    disp(' ');
    
    fprintf (fid, '\n');
    fprintf (fid, '                     Initial Condition ROEs\n');
    fprintf (fid, '(NOTE:  Values are for the singular vector that yielded the minimum');
    fprintf (fid, '        total residual RMS error computed using the specified method to determine alpha.)');
    fprintf(fid,   ' Source  :  True ICs            Line Search        Line Search      Mean Alpha         Mean Alpha');
    fprintf(fid, '                                (unfiltered)        (filtered)      (unfiltered)         (filtered)');
    fprintf (fid,'ae:     %e        %e       %e        %e\n', a_IC, a_search, a_search_f, a_SV, a_SV_filtered);
    fprintf (fid, 'xd:     %e        %e       %e        %e\n', x_IC, x_search, x_search_f, x_SV, x_SV_filtered);
    fprintf (fid, 'yd:     %e        %e       %e        %e\n', y_IC, y_search, y_search_f, y_SV, y_SV_filtered);
    fprintf (fid, 'Beta:   %e     %e      %e        %e\n', Beta_IC, Beta_search, Beta_search_f, Beta_SV, Beta_SV_filtered);
    fprintf (fid, 'z_max:  %e        %e       %e        %e\n', z_max_IC, z_max_search, z_max_search_f, z_max_SV, z_max_SV_filtered);
    fprintf (fid, 'gamma:  %e        %e       %e        %e\n', gamma_IC, gamma_search, gamma_search_f, gamma_SV, gamma_SV_filtered);
    fprintf (fid, '\n');
    
    %---Write ICs to output table:
    disp('                     Initial State Vector');
    disp('(NOTE:  Values are for the singular vector that yielded the minimum');
    disp('        total residual RMS error computed using the specified method to determine alpha.)');
    disp(  'Source  :  True ICs          Line Search        Line Search        Mean Alpha         Mean Alpha');
    disp('                              (unfiltered)       (filtered)        (unfiltered)       (filtered)');
    fprintf ('x0:    %e        %e       %e      %e      %e\n', IC(1), X0_minRMS(1,Imin_search), X0_minRMS_f(1,Imin_search_f), solvedIC_SV(1,Imin), solvedIC_SV_filtered(1,Imin_filtered));
    fprintf ('y0:     %e        %e        %e       %e        %e\n', IC(2), X0_minRMS(2,Imin_search), X0_minRMS_f(2,Imin_search_f), solvedIC_SV(2,Imin), solvedIC_SV_filtered(2,Imin_filtered));
    fprintf ('z0:     %e        %e        %e       %e        %e\n', IC(3), X0_minRMS(3,Imin_search), X0_minRMS_f(3,Imin_search_f), solvedIC_SV(3,Imin), solvedIC_SV_filtered(3,Imin_filtered));
    fprintf ('xdot0:  %e       %e      %e       %e       %e\n', IC(4), X0_minRMS(4,Imin_search), X0_minRMS_f(4,Imin_search_f), solvedIC_SV(4,Imin), solvedIC_SV_filtered(4,Imin_filtered));
    fprintf ('ydot0:  %e        %e        %e       %e        %e\n', IC(5), X0_minRMS(5,Imin_search), X0_minRMS_f(5,Imin_search_f), solvedIC_SV(5,Imin), solvedIC_SV_filtered(5,Imin_filtered));
    fprintf ('zdot0:  %e        %e        %e       %e        %e\n', IC(6), X0_minRMS(6,Imin_search), X0_minRMS_f(6,Imin_search_f), solvedIC_SV(6,Imin), solvedIC_SV_filtered(6,Imin_filtered));
    disp(' ');
    
    fprintf (fid, '                     Initial State Vector\n');
    fprintf (fid, '(NOTE:  Values are for the singular vector that yielded the minimum');
    fprintf (fid, '        total residual RMS error computed using the specified method to determine alpha.)');
    fprintf (fid,   'Source  :  True ICs          Line Search(unfiltered)       Line Search(filtered)           Mean Alpha         Mean Alpha\n');
    fprintf (fid, 'x0:    %e      %e        %e      %e       %e\n', IC(1), X0_minRMS(1,Imin_search), X0_minRMS_f(1,Imin_search_f), solvedIC_SV(1,Imin), solvedIC_SV_filtered(1,Imin_filtered));
    fprintf (fid, 'y0:     %e        %e        %e       %e        %e\n', IC(2), X0_minRMS(2,Imin_search), X0_minRMS_f(2,Imin_search_f), solvedIC_SV(2,Imin), solvedIC_SV_filtered(2,Imin_filtered));
    fprintf (fid, 'z0:     %e        %e        %e       %e        %e\n', IC(3), X0_minRMS(3,Imin_search), X0_minRMS_f(3,Imin_search_f), solvedIC_SV(3,Imin), solvedIC_SV_filtered(3,Imin_filtered));
    fprintf (fid, 'xdot0:  %e     %e        %e       %e       %e\n', IC(4), X0_minRMS(4,Imin_search), X0_minRMS_f(4,Imin_search_f), solvedIC_SV(4,Imin), solvedIC_SV_filtered(4,Imin_filtered));
    fprintf (fid, 'ydot0:  %e        %e        %e       %e        %e\n', IC(5), X0_minRMS(5,Imin_search), X0_minRMS_f(5,Imin_search_f), solvedIC_SV(5,Imin), solvedIC_SV_filtered(5,Imin_filtered));
    fprintf (fid, 'zdot0:  %e        %e        %e       %e        %e\n', IC(6), X0_minRMS(6,Imin_search), X0_minRMS_f(6,Imin_search_f), solvedIC_SV(6,Imin), solvedIC_SV_filtered(6,Imin_filtered));
    fprintf (fid, '\n');
    
    fprintf ('Total RSS Position Error (SIM vs SV w/mean alpha (unfiltered)):       %e\n', Position_RSS_Mean_Filtered);
    fprintf ('Total RSS Position Error (SIM vs SV w/mean alpha (filtered)):       %e\n', Position_RSS_Mean_Unfiltered);
    fprintf ('Total RSS Position Error (SIM vs SV w/line search (unfiltered)):       %e\n', Position_RSS_Line_Unfiltered);
    fprintf ('Total RSS Position Error (SIM vs SV w/line search (filtered)):       %e\n', Position_RSS_Line_Filtered);
    disp(' ');
        
    fprintf (fid, 'Total RSS Position Error (SIM vs SV w/mean alpha (unfiltered)):       %e\n', Position_RSS_Mean_Filtered);
    fprintf (fid, 'Total RSS Position Error (SIM vs SV w/mean alpha (filtered)):       %e\n', Position_RSS_Mean_Unfiltered);
    fprintf (fid, 'Total RSS Position Error (SIM vs SV w/line search (unfiltered)):       %e\n', Position_RSS_Line_Unfiltered);
    fprintf (fid, 'Total RSS Position Error (SIM vs SV w/line search (filtered)):       %e\n', Position_RSS_Line_Filtered);
    disp(' ');
        
    fprintf ('Total RMS Error of Observation Angle Residuals (SIM vs SV w/mean alpha (unfiltered)):       %e\n', RMSerr_SV(Imin));
    disp(' ');
    fprintf ('Total RMS Error of Observation Angle Residuals (SIM vs SV w/mean alpha (filtered)):         %e\n', RMSerr_SV_filtered(Imin_filtered));
    disp(' ');
    fprintf ('Total RMS Error of Observation Angle Residuals (SIM vs SV w/line search alpha):             %e\n', minRMS_search(Imin_search));
    disp(' ');
     fprintf ('Total RMS Error of Observation Angle Residuals (SIM vs SV w/filtered line search alpha):   %e\n', minRMS_search_f(Imin_search_f));
    disp(' ');
    fprintf ('Total RMS Error of Observation Angle Residuals (SIM vs QV-propagated):                      %e\n', RMSerr_SIMvsQV);
    disp(' ');
    
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (SIM vs SV w/mean alpha (unfiltered)):     %e\n', RMSerr_SV(Imin));
    disp(' ');
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (SIM vs SV w/mean alpha (filtered)):       %e\n', RMSerr_SV_filtered(Imin_filtered));
    disp(' ');
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (SIM vs SV w/line search alpha):           %e\n', minRMS_search(Imin_search));
    disp(' ');
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (SIM vs SV w/filtered line search alpha):  %e\n', minRMS_search_f(Imin_search_f));
    disp(' ');
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (SIM vs QV-propagated):                    %e\n', RMSerr_SIMvsQV);
%     fprintf (fid, '\n');

    fprintf ('statemagratio_SV (mean alpha unfiltered):             %e\n', statemagratio_SV);
    fprintf ('statemagratio_SV (mean alpha filtered):               %e\n', statemagratio_SV_filtered);
    fprintf ('statemagratio_search (line search alpha unfiltered):  %e\n', statemagratio_search(Imin_search));
    fprintf ('statemagratio_search (line search alpha filtered):    %e\n', statemagratio_search_f(Imin_search_f));
    disp(' ');
    
    fprintf (fid, 'statemagratio_SV (mean alpha unfiltered):            %e\n', statemagratio_SV);
    fprintf (fid, 'statemagratio_SV (mean alpha filtered):              %e\n', statemagratio_SV_filtered);
    fprintf (fid, 'statemagratio_search (line search alpha unfiltered): %e\n', statemagratio_search(Imin_search));
    fprintf (fid, 'statemagratio_search (line search alpha filtered):   %e\n', statemagratio_search_f(Imin_search_f));
    fprintf (fid, '\n');

    fprintf ('statedir_ratio_SV (mean alpha unfiltered):             %e\n', statedir_ratio_SV);
    fprintf ('statedir_ratio_SV (mean alpha filtered):               %e\n', statedir_ratio_SV_filtered);
    fprintf ('statedir_ratio_search (line search alpha unfiltered):  %e\n', statedir_ratio_search(Imin_search));
    fprintf ('statedir_ratio_search (line search alpha filtered):    %e\n', statedir_ratio_search_f(Imin_search_f));
    disp(' ');
    
    fprintf (fid, 'statedir_ratio_SV (mean alpha unfiltered):            %e\n', statedir_ratio_SV);
    fprintf (fid, 'statedir_ratio_SV (mean alpha filtered):              %e\n', statedir_ratio_SV_filtered);
    fprintf (fid, 'statedir_ratio_search (line search alpha unfiltered): %e\n', statedir_ratio_search(Imin_search));
    fprintf (fid, 'statedir_ratio_search (line search alpha filtered):   %e\n', statedir_ratio_search_f(Imin_search_f));
    fprintf (fid, '\n');

    fprintf ('range_RMSscale_SV (mean alpha unfiltered):             %e\n',range_RMSscale_SV);
    fprintf ('range_RMSscale_SV (mean alpha filtered):               %e\n',range_RMSscale_SV_filtered);
    fprintf ('range_RMSscale_search (line search alpha unfiltered):  %e\n',range_RMSscale_search(Imin_search));
     fprintf ('range_RMSscale_search (line search alpha filtered):   %e\n',range_RMSscale_search_f(Imin_search_f));
    fprintf ('range_RMSscale_QV:     %e\n', range_RMSscale_QV);
    
    fprintf (fid, 'range_RMSscale_SV (mean alpha unfiltered):            %e\n', range_RMSscale_SV);
    fprintf (fid, 'range_RMSscale_SV (mean alpha filtered):              %e\n', range_RMSscale_SV_filtered);
    fprintf (fid, 'range_RMSscale_search (line search alpha unfiltered): %e\n', range_RMSscale_search(Imin_search));
    fprintf (fid, 'range_RMSscale_search (line search alpha filtered):    %e\n',range_RMSscale_search_f(Imin_search_f));
    fprintf (fid, 'range_RMSscale_QV:                                     %e\n',range_RMSscale_QV);
 
end

%--------------------------------------------------------------------------
%---Output for real data only:
if data_type == 2
    
    disp(' ');
    disp('                     Initial Condition ROEs');
    disp('(NOTE:  Values are for the singular vector that yielded the minimum');
    disp('        total residual RMS error computed using the specified method to determine alpha.');
    disp ('Source: Line Search            Line Search            Mean Alpha            Mean Alpha');
    disp('         (unfiltered)            (filtered)          (unfiltered)            (filtered)');
    fprintf ('ae:     %e          %e          %e          %e\n', a_search, a_search_f, a_SV, a_SV_filtered);
    fprintf ('xd:     %e          %e          %e          %e\n', x_search, x_search_f, x_SV, x_SV_filtered);
    fprintf ('yd:     %e          %e          %e          %e\n', y_search, y_search_f, y_SV, y_SV_filtered);
    fprintf ('Beta:   %e          %e          %e          %e\n', Beta_search, Beta_search_f, Beta_SV, Beta_SV_filtered);
    fprintf ('z_max:  %e          %e          %e          %e\n', z_max_search, z_max_search_f, z_max_SV, z_max_SV_filtered);
    fprintf ('gamma:  %e          %e          %e          %e\n', gamma_search, gamma_search_f, gamma_SV, gamma_SV_filtered);
    disp(' ');
    
    fprintf (fid, '\n');
    fprintf (fid, '                     Initial Condition ROEs\n');
    fprintf (fid, '(NOTE:  Values are for the singular vector that yielded the minimum');
    fprintf (fid, '        total residual RMS error computed using the specified method to determine alpha.');
    fprintf (fid, 'Source: Line Search            Line Search           Mean Alpha            Mean Alpha');
    fprintf (fid, '       (unfiltered)             (filtered)         (unfiltered)            (filtered)');
    fprintf (fid, 'ae:     %e          %e          %e          %e\n', a_search, a_search_f, a_SV, a_SV_filtered);
    fprintf (fid, 'xd:     %e          %e          %e          %e\n', x_search, x_search_f, x_SV, x_SV_filtered);
    fprintf (fid, 'yd:     %e          %e          %e          %e\n', y_search, y_search_f, y_SV, y_SV_filtered);
    fprintf (fid, 'Beta:   %e          %e          %e          %e\n', Beta_search, Beta_search_f, Beta_SV, Beta_SV_filtered);
    fprintf (fid, 'z_max:  %e          %e          %e          %e\n', z_max_search, z_max_search_f, z_max_SV, z_max_SV_filtered);
    fprintf (fid, 'gamma:  %e          %e          %e          %e\n', gamma_search, gamma_search_f, gamma_SV, gamma_SV_filtered);
    fprintf (fid, '\n');
    
    %---Write ICs to output table:
    disp('                     Initial State Vector');
    disp('(NOTE:  Values are for the singular vector that yielded the minimum');
    disp('        total residual RMS error computed using the specified method to determine alpha.');
    disp ('Source: Line Search         Line Search         Mean Alpha         Mean Alpha\n');
    disp('        (unfiltered)        (filtered)         (unfiltered)        (filtered)');
    fprintf ('x0:     %e       %e       %e       %e\n', X0_minRMS(1,Imin_search), X0_minRMS_f(1,Imin_search_f), solvedIC_SV(1,Imin), solvedIC_SV_filtered(1,Imin_filtered));
    fprintf ('y0:     %e       %e       %e       %e\n', X0_minRMS(2,Imin_search), X0_minRMS_f(2,Imin_search_f), solvedIC_SV(2,Imin), solvedIC_SV_filtered(2,Imin_filtered));
    fprintf ('z0:     %e       %e       %e       %e\n', X0_minRMS(3,Imin_search), X0_minRMS_f(3,Imin_search_f), solvedIC_SV(3,Imin), solvedIC_SV_filtered(3,Imin_filtered));
    fprintf ('xdot0:  %e       %e       %e       %e\n', X0_minRMS(4,Imin_search), X0_minRMS_f(4,Imin_search_f), solvedIC_SV(4,Imin), solvedIC_SV_filtered(4,Imin_filtered));
    fprintf ('ydot0:  %e       %e       %e       %e\n', X0_minRMS(5,Imin_search), X0_minRMS_f(5,Imin_search_f), solvedIC_SV(5,Imin), solvedIC_SV_filtered(5,Imin_filtered));
    fprintf ('zdot0:  %e       %e       %e       %e\n', X0_minRMS(6,Imin_search), X0_minRMS_f(6,Imin_search_f), solvedIC_SV(6,Imin), solvedIC_SV_filtered(6,Imin_filtered));
    disp(' ');
    
    fprintf (fid, '                     Initial State Vector\n');
    fprintf (fid, '(NOTE:  Values are for the singular vector that yielded the minimum');
    fprintf (fid, '        total residual RMS error computed using the specified method to determine alpha.');
    fprintf (fid, 'Source: Line Search         Line Search         Mean Alpha         Mean Alpha');
    fprintf (fid, '        (unfiltered)        (filtered)         (unfiltered)        (filtered)');
    fprintf (fid, 'x0:     %e       %e       %e       %e\n', X0_minRMS(1,Imin_search), X0_minRMS_f(1,Imin_search_f), solvedIC_SV(1,Imin), solvedIC_SV_filtered(1,Imin_filtered));
    fprintf (fid, 'y0:     %e       %e       %e       %e\n', X0_minRMS(2,Imin_search), X0_minRMS_f(2,Imin_search_f), solvedIC_SV(2,Imin), solvedIC_SV_filtered(2,Imin_filtered));
    fprintf (fid, 'z0:     %e       %e       %e       %e\n', X0_minRMS(3,Imin_search), X0_minRMS_f(3,Imin_search_f), solvedIC_SV(3,Imin), solvedIC_SV_filtered(3,Imin_filtered));
    fprintf (fid, 'xdot0:  %e       %e       %e       %e\n', X0_minRMS(4,Imin_search), X0_minRMS_f(4,Imin_search_f), solvedIC_SV(4,Imin), solvedIC_SV_filtered(4,Imin_filtered));
    fprintf (fid, 'ydot0:  %e       %e       %e       %e\n', X0_minRMS(5,Imin_search), X0_minRMS_f(5,Imin_search_f), solvedIC_SV(5,Imin), solvedIC_SV_filtered(5,Imin_filtered));
    fprintf (fid, 'zdot0:  %e       %e       %e       %e\n', X0_minRMS(6,Imin_search), X0_minRMS_f(6,Imin_search_f), solvedIC_SV(6,Imin), solvedIC_SV_filtered(6,Imin_filtered));
    fprintf (fid, '\n');

    fprintf ('Total RMS Error of Observation Angle Residuals (Real vs SV w/mean alpha (unfiltered)):         %e\n', RMSerr_SV(Imin));
    disp(' ');
    fprintf ('Total RMS Error of Observation Angle Residuals (Real vs SV w/mean alpha (filtered)):           %e\n', RMSerr_SV_filtered(Imin_filtered));
    disp(' ');
    fprintf ('Total RMS Error of Observation Angle Residuals (Real vs SV w/unfiltered line search alpha):     %e\n', minRMS_search(Imin_search));
    disp(' ');
    fprintf ('Total RMS Error of Observation Angle Residuals (Real vs SV w/filtered line search alpha):      %e\n', minRMS_search_f(Imin_search_f));
    disp(' ');
    
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (Real vs SV w/mean alpha (unfiltered)):      %e\n', RMSerr_SV(Imin));
    disp(' ');
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (Real vs SV w/mean alpha (filtered)):        %e\n', RMSerr_SV_filtered(Imin_filtered));
    disp(' ');
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (Real vs SV w/unfiltered line search alpha): %e\n', minRMS_search(Imin_search));
    disp(' ');
    fprintf (fid, 'Total RMS Error of Observation Angle Residuals (Real vs SV w/filtered line search alpha):   %e\n', minRMS_search_f(Imin_search_f));
    disp(' ');
    fprintf (fid, '\n');

end

%---Close output file:
fclose(fid);

%--------------------------------------------------------------------------
%% Create Plots

%---Plots for simulated data only:
if data_type == 1
    
    %---Set ICs for mean alpha (unfiltered) results:
    x0_ma = solvedIC_SV(1,Imin);
    y0_ma = solvedIC_SV(2,Imin);
    z0_ma = solvedIC_SV(3,Imin);
    vx0_ma = solvedIC_SV(4,Imin);
    vy0_ma = solvedIC_SV(5,Imin);
    vz0_ma = solvedIC_SV(6,Imin);
    
    %---Set ICs for mean alpha (filtered) results:
    x0_ma_f = solvedIC_SV_filtered(1,Imin_filtered);
    y0_ma_f = solvedIC_SV_filtered(2,Imin_filtered);
    z0_ma_f = solvedIC_SV_filtered(3,Imin_filtered);
    vx0_ma_f = solvedIC_SV_filtered(4,Imin_filtered);
    vy0_ma_f = solvedIC_SV_filtered(5,Imin_filtered);
    vz0_ma_f = solvedIC_SV_filtered(6,Imin_filtered);
    
    %---Set ICs for line search (unfiltered) results:
    x0_ls = X0_minRMS(1,Imin_search);
    y0_ls = X0_minRMS(2,Imin_search);
    z0_ls = X0_minRMS(3,Imin_search);
    vx0_ls = X0_minRMS(4,Imin_search);
    vy0_ls = X0_minRMS(5,Imin_search);
    vz0_ls = X0_minRMS(6,Imin_search);
    
    %---Set ICs for line search (filtered) results:
    x0_ls_f = X0_minRMS_f(1,Imin_search_f);
    y0_ls_f = X0_minRMS_f(2,Imin_search_f);
    z0_ls_f = X0_minRMS_f(3,Imin_search_f);
    vx0_ls_f = X0_minRMS_f(4,Imin_search_f);
    vy0_ls_f = X0_minRMS_f(5,Imin_search_f);
    vz0_ls_f = X0_minRMS_f(6,Imin_search_f);
    
    trk = (ti : dtrk : tf)';    % array of times for RK integration (sec)
    
    %---Insert intial conditions into the state matrix:
    x(1,1) = x0;
    x(1,2) = y0;
    x(1,3) = z0;
    x(1,4) = xdot0;
    x(1,5) = ydot0;
    x(1,6) = zdot0;
    
    %---Create array of relative states at each time point in the array of
    %   times for RK integration by propagating the two-body solution using 4th
    %   order Runge-Kutta:
     i = 1;
    while trk(i,1) < tf
        k1 = deriv(x(i,:));
        k2 = deriv(x(i,:) + k1*dtrk/2);
        k3 = deriv(x(i,:) + k2*dtrk/2);
        k4 = deriv(x(i,:) + k3*dtrk);
        
        x(i+1,:) = x(i,:) + (dtrk/6)*(k1+2*k2+2*k3+k4);
        i = i + 1;
    end
    
     %---Use QV equations of motion to propagate "true" ICs:
    x_vol = vol_sol(trk,n,Rc,x0,y0,z0,xdot0,ydot0,zdot0);
    
    %---Use QV equations of motion to propagate mean alpha (unfiltered)
    %   ICs:
    x_ma = vol_sol(trk,n,Rc,x0_ma,y0_ma,z0_ma,vx0_ma,vy0_ma,vz0_ma);

    %---Use QV equations of motion to propagate mean alpha (filtered)
    %   ICs:
    x_ma_f = vol_sol(trk,n,Rc,x0_ma_f,y0_ma_f,z0_ma_f,vx0_ma_f,vy0_ma_f,vz0_ma_f);
    
    %---Use QV equations of motion to propagate line search (unfiltered)
    %   ICs:
    x_ls = vol_sol(trk,n,Rc,x0_ls,y0_ls,z0_ls,vx0_ls,vy0_ls,vz0_ls);

     %---Use QV equations of motion to propagate line search (filtered)
    %   ICs:
    x_ls_f = vol_sol(trk,n,Rc,x0_ls_f,y0_ls_f,z0_ls_f,vx0_ls_f,vy0_ls_f,vz0_ls_f);

end
% % % %     
 %NOTE: For plots (1) through (6), one must manually adjust the LOS
 %inclusion if the number of observations is more than 15. Currently, each
 %plot will only include 15 LOS. 
   %---Plot 1:  x vs. y (RK w/IC, QV w/IC, trajectories with solutions from mean unfiltered,
   %mean filtered, line search unfiltered, and line search filtered)
   if data_type == 1
    figure(1)
    plot(x(:,2),x(:,1),'b','DisplayName','RK w/IC');hold on;
    plot(x_vol(:,2),x_vol(:,1),'g','DisplayName','QV w/IC');hold on;
    plot(x_ma(:,2),x_ma(:,1),'k','DisplayName','Mean Alpha (unfiltered)');hold on;
    plot(x_ma_f(:,2),x_ma_f(:,1),'c','DisplayName','Mean Alpha (filtered)');hold on;
    plot(x_ls(:,2),x_ls(:,1),'m','DisplayName','Line Search (unfiltered)');hold on;
    plot(x_ls_f(:,2),x_ls_f(:,1),'m','DisplayName','Line Search (filtered)');hold on;
    legend('Location','BestOutside');
    plot([0 yobs(1)],[0 xobs(1)],'r:',[0 yobs(2)],[0 xobs(2)],'r:',[0 yobs(3)],[0 xobs(3)],'r:',[0 yobs(4)],[0 xobs(4)],...
        'r:',[0 yobs(5)],[0 xobs(5)],'r:',[0 yobs(6)],[0 xobs(6)],'r:',[0 yobs(7)],[0 xobs(7)],'r:',[0 yobs(8)],[0 xobs(8)],...
        'r:',[0 yobs(9)],[0 xobs(9)],'r:',[0 yobs(10)],[0 xobs(10)],'r:',[0 yobs(11)],[0 xobs(11)],'r:',[0 yobs(12)],[0 xobs(12)],...
        'r:',[0 yobs(13)],[0 xobs(13)],'r:',[0 yobs(14)],[0 xobs(14)],'r:',...
            [0 yobs(15)],[0 xobs(15)],'r:');
%          [0 yobs(16)],[0 xobs(16)],'r:',[0 yobs(17)],[0 xobs(17)],'r:',[0 yobs(18)],[0 xobs(18)],'r:',[0 yobs(19)],[0 xobs(19)],'r:',[0 yobs(20)],[0 xobs(20)],'r:',...
%                         [0 yobs(21)],[0 xobs(21)],'r:',[0 yobs(22)],[0 xobs(22)],'r:',[0 yobs(23)],[0 xobs(23)],'r:',[0 yobs(24)],[0 xobs(24)],'r:',[0 yobs(25)],[0 xobs(25)],'r:',...
%                         [0 yobs(26)],[0 xobs(26)],'r:',[0 yobs(27)],[0 xobs(27)],'r:',[0 yobs(28)],[0 xobs(28)],'r:',[0 yobs(29)],[0 xobs(29)],'r:',[0 yobs(30)],[0 xobs(30)],'r:',...
%                         [0 yobs(31)],[0 xobs(31)],'r:',[0 yobs(32)],[0 xobs(32)],'r:',[0 yobs(33)],[0 xobs(33)],'r:',[0 yobs(34)],[0 xobs(34)],'r:',[0 yobs(35)],[0 xobs(35)],'r:',...
%                         [0 yobs(36)],[0 xobs(36)],'r:',[0 yobs(37)],[0 xobs(37)],'r:',[0 yobs(38)],[0 xobs(38)],'r:',[0 yobs(39)],[0 xobs(39)],'r:',[0 yobs(40)],[0 xobs(40)],'r:',...
%                         [0 yobs(41)],[0 xobs(41)],'r:',[0 yobs(42)],[0 xobs(42)],'r:',[0 yobs(43)],[0 xobs(43)],'r:',[0 yobs(44)],[0 xobs(44)],'r:',[0 yobs(45)],[0 xobs(45)],'r:',...
%                         [0 yobs(46)],[0 xobs(46)],'r:',[0 yobs(47)],[0 xobs(47)],'r:',[0 yobs(48)],[0 xobs(48)],'r:',[0 yobs(49)],[0 xobs(49)],'r:',[0 yobs(50)],[0 xobs(50)],'r:',...
%                         [0 yobs(51)],[0 xobs(51)],'r:',[0 yobs(52)],[0 xobs(52)],'r:',[0 yobs(53)],[0 xobs(53)],'r:',[0 yobs(54)],[0 xobs(54)],'r:',[0 yobs(55)],[0 xobs(55)],'r:',...
%                         [0 yobs(56)],[0 xobs(56)],'r:',[0 yobs(57)],[0 xobs(57)],'r:',[0 yobs(58)],[0 xobs(58)],'r:',[0 yobs(59)],[0 xobs(59)],'r:',[0 yobs(60)],[0 xobs(60)],'r:',...
%                         [0 yobs(61)],[0 xobs(61)],'r:',[0 yobs(62)],[0 xobs(62)],'r:',[0 yobs(63)],[0 xobs(63)],'r:',[0 yobs(64)],[0 xobs(64)],'r:',[0 yobs(65)],[0 xobs(65)],'r:',...
%                         [0 yobs(66)],[0 xobs(66)],'r:',[0 yobs(67)],[0 xobs(67)],'r:',[0 yobs(68)],[0 xobs(68)],'r:',[0 yobs(69)],[0 xobs(69)],'r:',[0 yobs(70)],[0 xobs(70)],'r:',...
%                         [0 yobs(71)],[0 xobs(71)],'r:',[0 yobs(72)],[0 xobs(72)],'r:',[0 yobs(73)],[0 xobs(73)],'r:',[0 yobs(74)],[0 xobs(74)],'r:',[0 yobs(75)],[0 xobs(75)],'r:',...
%                         [0 yobs(76)],[0 xobs(76)],'r:',[0 yobs(77)],[0 xobs(77)],'r:',[0 yobs(78)],[0 xobs(78)],'r:',[0 yobs(79)],[0 xobs(79)],'r:',[0 yobs(80)],[0 xobs(80)],'r:',...
%                         [0 yobs(81)],[0 xobs(81)],'r:',[0 yobs(82)],[0 xobs(82)],'r:',[0 yobs(83)],[0 xobs(83)],'r:',[0 yobs(84)],[0 xobs(84)],'r:',[0 yobs(85)],[0 xobs(85)],'r:',...
%                         [0 yobs(86)],[0 xobs(86)],'r:',[0 yobs(87)],[0 xobs(87)],'r:',[0 yobs(88)],[0 xobs(88)],'r:',[0 yobs(89)],[0 xobs(89)],'r:',[0 yobs(90)],[0 xobs(90)],'r:',...
%                         [0 yobs(91)],[0 xobs(91)],'r:',[0 yobs(92)],[0 xobs(92)],'r:',[0 yobs(93)],[0 xobs(93)],'r:',[0 yobs(94)],[0 xobs(94)],'r:',[0 yobs(95)],[0 xobs(95)],'r:',...
%                         [0 yobs(96)],[0 xobs(96)],'r:',[0 yobs(97)],[0 xobs(97)],'r:',[0 yobs(98)],[0 xobs(98)],'r:',[0 yobs(99)],[0 xobs(99)],'r:',[0 yobs(100)],[0 xobs(100)],'r:');
%     axis equal
    title('x vs y');
    xlabel('y (km)')
    ylabel('x (km)')

    %---Plot 2: x vs. y (RK and QV with IC)
    figure(2)
    plot(x(:,2),x(:,1),'g','DisplayName','RK with IC');hold on;
    plot(x_vol(:,2),x_vol(:,1),'b','DisplayName','QV with IC');hold on;
    legend('Location','BestOutside');
    plot([0 yobs(1)],[0 xobs(1)],'r:',[0 yobs(2)],[0 xobs(2)],'r:',[0 yobs(3)],[0 xobs(3)],'r:',[0 yobs(4)],[0 xobs(4)],'r:',[0 yobs(5)],[0 xobs(5)],'r:',[0 yobs(6)],[0 xobs(6)],'r:',[0 yobs(7)],[0 xobs(7)],'r:',[0 yobs(8)],[0 xobs(8)],'r:',[0 yobs(9)],[0 xobs(9)],'r:',[0 yobs(10)],[0 xobs(10)],'r:',[0 yobs(11)],[0 xobs(11)],'r:',[0 yobs(12)],[0 xobs(12)],'r:',[0 yobs(13)],[0 xobs(13)],'r:',[0 yobs(14)],[0 xobs(14)],'r:',[0 yobs(15)],[0 xobs(15)],'r:', ...
        [0 yobs(15)],[0 xobs(15)],'r:');
%      [0 yobs(16)],[0 xobs(16)],'r:',[0 yobs(17)],[0 xobs(17)],'r:',[0 yobs(18)],[0 xobs(18)],'r:',[0 yobs(19)],[0 xobs(19)],'r:',[0 yobs(20)],[0 xobs(20)],'r:',...
%                    [0 yobs(21)],[0 xobs(21)],'r:',[0 yobs(22)],[0 xobs(22)],'r:',[0 yobs(23)],[0 xobs(23)],'r:',[0 yobs(24)],[0 xobs(24)],'r:',[0 yobs(25)],[0 xobs(25)],'r:',...
%                         [0 yobs(26)],[0 xobs(26)],'r:',[0 yobs(27)],[0 xobs(27)],'r:',[0 yobs(28)],[0 xobs(28)],'r:',[0 yobs(29)],[0 xobs(29)],'r:',[0 yobs(30)],[0 xobs(30)],'r:',...
%                         [0 yobs(31)],[0 xobs(31)],'r:',[0 yobs(32)],[0 xobs(32)],'r:',[0 yobs(33)],[0 xobs(33)],'r:',[0 yobs(34)],[0 xobs(34)],'r:',[0 yobs(35)],[0 xobs(35)],'r:',...
%                         [0 yobs(36)],[0 xobs(36)],'r:',[0 yobs(37)],[0 xobs(37)],'r:',[0 yobs(38)],[0 xobs(38)],'r:',[0 yobs(39)],[0 xobs(39)],'r:',[0 yobs(40)],[0 xobs(40)],'r:',...
%                         [0 yobs(41)],[0 xobs(41)],'r:',[0 yobs(42)],[0 xobs(42)],'r:',[0 yobs(43)],[0 xobs(43)],'r:',[0 yobs(44)],[0 xobs(44)],'r:',[0 yobs(45)],[0 xobs(45)],'r:',...
%                         [0 yobs(46)],[0 xobs(46)],'r:',[0 yobs(47)],[0 xobs(47)],'r:',[0 yobs(48)],[0 xobs(48)],'r:',[0 yobs(49)],[0 xobs(49)],'r:',[0 yobs(50)],[0 xobs(50)],'r:',...
%                         [0 yobs(51)],[0 xobs(51)],'r:',[0 yobs(52)],[0 xobs(52)],'r:',[0 yobs(53)],[0 xobs(53)],'r:',[0 yobs(54)],[0 xobs(54)],'r:',[0 yobs(55)],[0 xobs(55)],'r:',...
%                         [0 yobs(56)],[0 xobs(56)],'r:',[0 yobs(57)],[0 xobs(57)],'r:',[0 yobs(58)],[0 xobs(58)],'r:',[0 yobs(59)],[0 xobs(59)],'r:',[0 yobs(60)],[0 xobs(60)],'r:',...
%                         [0 yobs(61)],[0 xobs(61)],'r:',[0 yobs(62)],[0 xobs(62)],'r:',[0 yobs(63)],[0 xobs(63)],'r:',[0 yobs(64)],[0 xobs(64)],'r:',[0 yobs(65)],[0 xobs(65)],'r:',...
%                         [0 yobs(66)],[0 xobs(66)],'r:',[0 yobs(67)],[0 xobs(67)],'r:',[0 yobs(68)],[0 xobs(68)],'r:',[0 yobs(69)],[0 xobs(69)],'r:',[0 yobs(70)],[0 xobs(70)],'r:',...
%                         [0 yobs(71)],[0 xobs(71)],'r:',[0 yobs(72)],[0 xobs(72)],'r:',[0 yobs(73)],[0 xobs(73)],'r:',[0 yobs(74)],[0 xobs(74)],'r:',[0 yobs(75)],[0 xobs(75)],'r:',...
%                         [0 yobs(76)],[0 xobs(76)],'r:',[0 yobs(77)],[0 xobs(77)],'r:',[0 yobs(78)],[0 xobs(78)],'r:',[0 yobs(79)],[0 xobs(79)],'r:',[0 yobs(80)],[0 xobs(80)],'r:',...
%                         [0 yobs(81)],[0 xobs(81)],'r:',[0 yobs(82)],[0 xobs(82)],'r:',[0 yobs(83)],[0 xobs(83)],'r:',[0 yobs(84)],[0 xobs(84)],'r:',[0 yobs(85)],[0 xobs(85)],'r:',...
%                         [0 yobs(86)],[0 xobs(86)],'r:',[0 yobs(87)],[0 xobs(87)],'r:',[0 yobs(88)],[0 xobs(88)],'r:',[0 yobs(89)],[0 xobs(89)],'r:',[0 yobs(90)],[0 xobs(90)],'r:',...
%                         [0 yobs(91)],[0 xobs(91)],'r:',[0 yobs(92)],[0 xobs(92)],'r:',[0 yobs(93)],[0 xobs(93)],'r:',[0 yobs(94)],[0 xobs(94)],'r:',[0 yobs(95)],[0 xobs(95)],'r:',...
%                         [0 yobs(96)],[0 xobs(96)],'r:',[0 yobs(97)],[0 xobs(97)],'r:',[0 yobs(98)],[0 xobs(98)],'r:',[0 yobs(99)],[0 xobs(99)],'r:',[0 yobs(100)],[0 xobs(100)],'r:');

    axis equal
    title('x vs y');
    xlabel('y (km)')
    ylabel('x (km)') 
 
    %---Plot 3: x vs. y (Mean unfiltered trajectory and QV with IC, QV-predicted states of Mean Unfiltered at
    %   measurement times:) 
    figure(3)
    plot(x_ma(:,2),x_ma(:,1),'g','DisplayName','Mean Unfilt');hold on;
    plot(x_vol(:,2),x_vol(:,1),'b','DisplayName','QV w/IC');hold on;
    plot(x_vol_SV(:,2,Imin),x_vol_SV(:,1,Imin),'kx','MarkerSize',8,'DisplayName','Predicted States'); hold on;
    legend('Location','BestOutside');
    plot([0 yobs(1)],[0 xobs(1)],'r:',[0 yobs(2)],[0 xobs(2)],'r:',[0 yobs(3)],[0 xobs(3)],'r:',[0 yobs(4)],[0 xobs(4)],'r:',[0 yobs(5)],[0 xobs(5)],'r:',[0 yobs(6)],[0 xobs(6)],'r:',[0 yobs(7)],[0 xobs(7)],'r:',[0 yobs(8)],[0 xobs(8)],'r:',[0 yobs(9)],[0 xobs(9)],'r:',[0 yobs(10)],[0 xobs(10)],'r:',[0 yobs(11)],[0 xobs(11)],'r:',[0 yobs(12)],[0 xobs(12)],'r:',[0 yobs(13)],[0 xobs(13)],'r:',[0 yobs(14)],[0 xobs(14)],'r:',...
            [0 yobs(15)],[0 xobs(15)],'r:');
%         [0 yobs(16)],[0 xobs(16)],'r:',[0 yobs(17)],[0 xobs(17)],'r:',[0 yobs(18)],[0 xobs(18)],'r:',[0 yobs(19)],[0 xobs(19)],'r:',[0 yobs(20)],[0 xobs(20)],'r:',...
%                         [0 yobs(21)],[0 xobs(21)],'r:',[0 yobs(22)],[0 xobs(22)],'r:',[0 yobs(23)],[0 xobs(23)],'r:',[0 yobs(24)],[0 xobs(24)],'r:',[0 yobs(25)],[0 xobs(25)],'r:',...
%                         [0 yobs(26)],[0 xobs(26)],'r:',[0 yobs(27)],[0 xobs(27)],'r:',[0 yobs(28)],[0 xobs(28)],'r:',[0 yobs(29)],[0 xobs(29)],'r:',[0 yobs(30)],[0 xobs(30)],'r:',...
%                         [0 yobs(31)],[0 xobs(31)],'r:',[0 yobs(32)],[0 xobs(32)],'r:',[0 yobs(33)],[0 xobs(33)],'r:',[0 yobs(34)],[0 xobs(34)],'r:',[0 yobs(35)],[0 xobs(35)],'r:',...
%                         [0 yobs(36)],[0 xobs(36)],'r:',[0 yobs(37)],[0 xobs(37)],'r:',[0 yobs(38)],[0 xobs(38)],'r:',[0 yobs(39)],[0 xobs(39)],'r:',[0 yobs(40)],[0 xobs(40)],'r:',...
%                         [0 yobs(41)],[0 xobs(41)],'r:',[0 yobs(42)],[0 xobs(42)],'r:',[0 yobs(43)],[0 xobs(43)],'r:',[0 yobs(44)],[0 xobs(44)],'r:',[0 yobs(45)],[0 xobs(45)],'r:',...
%                         [0 yobs(46)],[0 xobs(46)],'r:',[0 yobs(47)],[0 xobs(47)],'r:',[0 yobs(48)],[0 xobs(48)],'r:',[0 yobs(49)],[0 xobs(49)],'r:',[0 yobs(50)],[0 xobs(50)],'r:',...
%                         [0 yobs(51)],[0 xobs(51)],'r:',[0 yobs(52)],[0 xobs(52)],'r:',[0 yobs(53)],[0 xobs(53)],'r:',[0 yobs(54)],[0 xobs(54)],'r:',[0 yobs(55)],[0 xobs(55)],'r:',...
%                         [0 yobs(56)],[0 xobs(56)],'r:',[0 yobs(57)],[0 xobs(57)],'r:',[0 yobs(58)],[0 xobs(58)],'r:',[0 yobs(59)],[0 xobs(59)],'r:',[0 yobs(60)],[0 xobs(60)],'r:',...
%                         [0 yobs(61)],[0 xobs(61)],'r:',[0 yobs(62)],[0 xobs(62)],'r:',[0 yobs(63)],[0 xobs(63)],'r:',[0 yobs(64)],[0 xobs(64)],'r:',[0 yobs(65)],[0 xobs(65)],'r:',...
%                         [0 yobs(66)],[0 xobs(66)],'r:',[0 yobs(67)],[0 xobs(67)],'r:',[0 yobs(68)],[0 xobs(68)],'r:',[0 yobs(69)],[0 xobs(69)],'r:',[0 yobs(70)],[0 xobs(70)],'r:',...
%                         [0 yobs(71)],[0 xobs(71)],'r:',[0 yobs(72)],[0 xobs(72)],'r:',[0 yobs(73)],[0 xobs(73)],'r:',[0 yobs(74)],[0 xobs(74)],'r:',[0 yobs(75)],[0 xobs(75)],'r:',...
%                         [0 yobs(76)],[0 xobs(76)],'r:',[0 yobs(77)],[0 xobs(77)],'r:',[0 yobs(78)],[0 xobs(78)],'r:',[0 yobs(79)],[0 xobs(79)],'r:',[0 yobs(80)],[0 xobs(80)],'r:',...
%                         [0 yobs(81)],[0 xobs(81)],'r:',[0 yobs(82)],[0 xobs(82)],'r:',[0 yobs(83)],[0 xobs(83)],'r:',[0 yobs(84)],[0 xobs(84)],'r:',[0 yobs(85)],[0 xobs(85)],'r:',...
%                         [0 yobs(86)],[0 xobs(86)],'r:',[0 yobs(87)],[0 xobs(87)],'r:',[0 yobs(88)],[0 xobs(88)],'r:',[0 yobs(89)],[0 xobs(89)],'r:',[0 yobs(90)],[0 xobs(90)],'r:',...
%                         [0 yobs(91)],[0 xobs(91)],'r:',[0 yobs(92)],[0 xobs(92)],'r:',[0 yobs(93)],[0 xobs(93)],'r:',[0 yobs(94)],[0 xobs(94)],'r:',[0 yobs(95)],[0 xobs(95)],'r:',...
%                         [0 yobs(96)],[0 xobs(96)],'r:',[0 yobs(97)],[0 xobs(97)],'r:',[0 yobs(98)],[0 xobs(98)],'r:',[0 yobs(99)],[0 xobs(99)],'r:',[0 yobs(100)],[0 xobs(100)],'r:');

    axis equal
    title('x vs y');
    xlabel('y (km)')
    ylabel('x (km)')
    
     
    %---Plot 4: x vs. y (Mean filtered trajectory and QV with IC, QV-predicted states of Mean filtered at
    %   measurement times:) 
    figure(4)
    plot(x_ma_f(:,2),x_ma_f(:,1),'g','DisplayName','Mean Filt');hold on;
    plot(x_vol(:,2),x_vol(:,1),'b','DisplayName','QV w/IC');hold on;
    plot(x_vol_SV_filtered(:,2,Imin_filtered),x_vol_SV_filtered(:,1,Imin_filtered),'kx','MarkerSize',8,'DisplayName','Predicted States'); hold on;
    legend('Location','BestOutside');
    plot([0 yobs(1)],[0 xobs(1)],'r:',[0 yobs(2)],[0 xobs(2)],'r:',[0 yobs(3)],[0 xobs(3)],'r:',[0 yobs(4)],[0 xobs(4)],'r:',[0 yobs(5)],[0 xobs(5)],'r:',[0 yobs(6)],[0 xobs(6)],'r:',[0 yobs(7)],[0 xobs(7)],'r:',[0 yobs(8)],[0 xobs(8)],'r:',[0 yobs(9)],[0 xobs(9)],'r:',[0 yobs(10)],[0 xobs(10)],'r:',[0 yobs(11)],[0 xobs(11)],'r:',[0 yobs(12)],[0 xobs(12)],'r:',[0 yobs(13)],[0 xobs(13)],'r:',[0 yobs(14)],[0 xobs(14)],'r:',...
            [0 yobs(15)],[0 xobs(15)],'r:');
%         [0 yobs(16)],[0 xobs(16)],'r:',[0 yobs(17)],[0 xobs(17)],'r:',[0 yobs(18)],[0 xobs(18)],'r:',[0 yobs(19)],[0 xobs(19)],'r:',[0 yobs(20)],[0 xobs(20)],'r:',...
%                         [0 yobs(21)],[0 xobs(21)],'r:',[0 yobs(22)],[0 xobs(22)],'r:',[0 yobs(23)],[0 xobs(23)],'r:',[0 yobs(24)],[0 xobs(24)],'r:',[0 yobs(25)],[0 xobs(25)],'r:',...
%                         [0 yobs(26)],[0 xobs(26)],'r:',[0 yobs(27)],[0 xobs(27)],'r:',[0 yobs(28)],[0 xobs(28)],'r:',[0 yobs(29)],[0 xobs(29)],'r:',[0 yobs(30)],[0 xobs(30)],'r:',...
%                         [0 yobs(31)],[0 xobs(31)],'r:',[0 yobs(32)],[0 xobs(32)],'r:',[0 yobs(33)],[0 xobs(33)],'r:',[0 yobs(34)],[0 xobs(34)],'r:',[0 yobs(35)],[0 xobs(35)],'r:',...
%                         [0 yobs(36)],[0 xobs(36)],'r:',[0 yobs(37)],[0 xobs(37)],'r:',[0 yobs(38)],[0 xobs(38)],'r:',[0 yobs(39)],[0 xobs(39)],'r:',[0 yobs(40)],[0 xobs(40)],'r:',...
%                         [0 yobs(41)],[0 xobs(41)],'r:',[0 yobs(42)],[0 xobs(42)],'r:',[0 yobs(43)],[0 xobs(43)],'r:',[0 yobs(44)],[0 xobs(44)],'r:',[0 yobs(45)],[0 xobs(45)],'r:',...
%                         [0 yobs(46)],[0 xobs(46)],'r:',[0 yobs(47)],[0 xobs(47)],'r:',[0 yobs(48)],[0 xobs(48)],'r:',[0 yobs(49)],[0 xobs(49)],'r:',[0 yobs(50)],[0 xobs(50)],'r:',...
%                         [0 yobs(51)],[0 xobs(51)],'r:',[0 yobs(52)],[0 xobs(52)],'r:',[0 yobs(53)],[0 xobs(53)],'r:',[0 yobs(54)],[0 xobs(54)],'r:',[0 yobs(55)],[0 xobs(55)],'r:',...
%                         [0 yobs(56)],[0 xobs(56)],'r:',[0 yobs(57)],[0 xobs(57)],'r:',[0 yobs(58)],[0 xobs(58)],'r:',[0 yobs(59)],[0 xobs(59)],'r:',[0 yobs(60)],[0 xobs(60)],'r:',...
%                         [0 yobs(61)],[0 xobs(61)],'r:',[0 yobs(62)],[0 xobs(62)],'r:',[0 yobs(63)],[0 xobs(63)],'r:',[0 yobs(64)],[0 xobs(64)],'r:',[0 yobs(65)],[0 xobs(65)],'r:',...
%                         [0 yobs(66)],[0 xobs(66)],'r:',[0 yobs(67)],[0 xobs(67)],'r:',[0 yobs(68)],[0 xobs(68)],'r:',[0 yobs(69)],[0 xobs(69)],'r:',[0 yobs(70)],[0 xobs(70)],'r:',...
%                         [0 yobs(71)],[0 xobs(71)],'r:',[0 yobs(72)],[0 xobs(72)],'r:',[0 yobs(73)],[0 xobs(73)],'r:',[0 yobs(74)],[0 xobs(74)],'r:',[0 yobs(75)],[0 xobs(75)],'r:',...
%                         [0 yobs(76)],[0 xobs(76)],'r:',[0 yobs(77)],[0 xobs(77)],'r:',[0 yobs(78)],[0 xobs(78)],'r:',[0 yobs(79)],[0 xobs(79)],'r:',[0 yobs(80)],[0 xobs(80)],'r:',...
%                         [0 yobs(81)],[0 xobs(81)],'r:',[0 yobs(82)],[0 xobs(82)],'r:',[0 yobs(83)],[0 xobs(83)],'r:',[0 yobs(84)],[0 xobs(84)],'r:',[0 yobs(85)],[0 xobs(85)],'r:',...
%                         [0 yobs(86)],[0 xobs(86)],'r:',[0 yobs(87)],[0 xobs(87)],'r:',[0 yobs(88)],[0 xobs(88)],'r:',[0 yobs(89)],[0 xobs(89)],'r:',[0 yobs(90)],[0 xobs(90)],'r:',...
%                         [0 yobs(91)],[0 xobs(91)],'r:',[0 yobs(92)],[0 xobs(92)],'r:',[0 yobs(93)],[0 xobs(93)],'r:',[0 yobs(94)],[0 xobs(94)],'r:',[0 yobs(95)],[0 xobs(95)],'r:',...
%                         [0 yobs(96)],[0 xobs(96)],'r:',[0 yobs(97)],[0 xobs(97)],'r:',[0 yobs(98)],[0 xobs(98)],'r:',[0 yobs(99)],[0 xobs(99)],'r:',[0 yobs(100)],[0 xobs(100)],'r:');


    axis equal
    title('x vs y');
    xlabel('y (km)')
    ylabel('x (km)')
    
     
    %---Plot 5: x vs. y (Line search unfiltered trajectory and QV with IC, QV-predicted states of Line search unfiltered at
    %   measurement times:) 
    figure(5)
    plot(x_ls(:,2),x_ls(:,1),'g','DisplayName','Line Unfilt');hold on;
    plot(x_vol(:,2),x_vol(:,1),'b','DisplayName','QV with IC');hold on;
    plot(x_vol_line_search(:,2,Imin_search),x_vol_line_search(:,1,Imin_search),'kx','MarkerSize',8, 'DisplayName','Predicted States'); hold on;
    legend('Location','BestOutside');
    plot([0 yobs(1)],[0 xobs(1)],'r:',[0 yobs(2)],[0 xobs(2)],'r:',[0 yobs(3)],[0 xobs(3)],'r:',[0 yobs(4)],[0 xobs(4)],'r:',[0 yobs(5)],[0 xobs(5)],'r:',[0 yobs(6)],[0 xobs(6)],'r:',[0 yobs(7)],[0 xobs(7)],'r:',[0 yobs(8)],[0 xobs(8)],'r:',[0 yobs(9)],[0 xobs(9)],'r:',[0 yobs(10)],[0 xobs(10)],'r:',[0 yobs(11)],[0 xobs(11)],'r:',[0 yobs(12)],[0 xobs(12)],'r:',[0 yobs(13)],[0 xobs(13)],'r:',[0 yobs(14)],[0 xobs(14)],'r:',...
            [0 yobs(15)],[0 xobs(15)],'r:');
%         [0 yobs(16)],[0 xobs(16)],'r:',[0 yobs(17)],[0 xobs(17)],'r:',[0 yobs(18)],[0 xobs(18)],'r:',[0 yobs(19)],[0 xobs(19)],'r:',[0 yobs(20)],[0 xobs(20)],'r:',...
%                 [0 yobs(21)],[0 xobs(21)],'r:',[0 yobs(22)],[0 xobs(22)],'r:',[0 yobs(23)],[0 xobs(23)],'r:',[0 yobs(24)],[0 xobs(24)],'r:',[0 yobs(25)],[0 xobs(25)],'r:',...
%                         [0 yobs(26)],[0 xobs(26)],'r:',[0 yobs(27)],[0 xobs(27)],'r:',[0 yobs(28)],[0 xobs(28)],'r:',[0 yobs(29)],[0 xobs(29)],'r:',[0 yobs(30)],[0 xobs(30)],'r:',...
%                         [0 yobs(31)],[0 xobs(31)],'r:',[0 yobs(32)],[0 xobs(32)],'r:',[0 yobs(33)],[0 xobs(33)],'r:',[0 yobs(34)],[0 xobs(34)],'r:',[0 yobs(35)],[0 xobs(35)],'r:',...
%                         [0 yobs(36)],[0 xobs(36)],'r:',[0 yobs(37)],[0 xobs(37)],'r:',[0 yobs(38)],[0 xobs(38)],'r:',[0 yobs(39)],[0 xobs(39)],'r:',[0 yobs(40)],[0 xobs(40)],'r:',...
%                         [0 yobs(41)],[0 xobs(41)],'r:',[0 yobs(42)],[0 xobs(42)],'r:',[0 yobs(43)],[0 xobs(43)],'r:',[0 yobs(44)],[0 xobs(44)],'r:',[0 yobs(45)],[0 xobs(45)],'r:',...
%                         [0 yobs(46)],[0 xobs(46)],'r:',[0 yobs(47)],[0 xobs(47)],'r:',[0 yobs(48)],[0 xobs(48)],'r:',[0 yobs(49)],[0 xobs(49)],'r:',[0 yobs(50)],[0 xobs(50)],'r:',...
%                         [0 yobs(51)],[0 xobs(51)],'r:',[0 yobs(52)],[0 xobs(52)],'r:',[0 yobs(53)],[0 xobs(53)],'r:',[0 yobs(54)],[0 xobs(54)],'r:',[0 yobs(55)],[0 xobs(55)],'r:',...
%                         [0 yobs(56)],[0 xobs(56)],'r:',[0 yobs(57)],[0 xobs(57)],'r:',[0 yobs(58)],[0 xobs(58)],'r:',[0 yobs(59)],[0 xobs(59)],'r:',[0 yobs(60)],[0 xobs(60)],'r:',...
%                         [0 yobs(61)],[0 xobs(61)],'r:',[0 yobs(62)],[0 xobs(62)],'r:',[0 yobs(63)],[0 xobs(63)],'r:',[0 yobs(64)],[0 xobs(64)],'r:',[0 yobs(65)],[0 xobs(65)],'r:',...
%                         [0 yobs(66)],[0 xobs(66)],'r:',[0 yobs(67)],[0 xobs(67)],'r:',[0 yobs(68)],[0 xobs(68)],'r:',[0 yobs(69)],[0 xobs(69)],'r:',[0 yobs(70)],[0 xobs(70)],'r:',...
%                         [0 yobs(71)],[0 xobs(71)],'r:',[0 yobs(72)],[0 xobs(72)],'r:',[0 yobs(73)],[0 xobs(73)],'r:',[0 yobs(74)],[0 xobs(74)],'r:',[0 yobs(75)],[0 xobs(75)],'r:',...
%                         [0 yobs(76)],[0 xobs(76)],'r:',[0 yobs(77)],[0 xobs(77)],'r:',[0 yobs(78)],[0 xobs(78)],'r:',[0 yobs(79)],[0 xobs(79)],'r:',[0 yobs(80)],[0 xobs(80)],'r:',...
%                         [0 yobs(81)],[0 xobs(81)],'r:',[0 yobs(82)],[0 xobs(82)],'r:',[0 yobs(83)],[0 xobs(83)],'r:',[0 yobs(84)],[0 xobs(84)],'r:',[0 yobs(85)],[0 xobs(85)],'r:',...
%                         [0 yobs(86)],[0 xobs(86)],'r:',[0 yobs(87)],[0 xobs(87)],'r:',[0 yobs(88)],[0 xobs(88)],'r:',[0 yobs(89)],[0 xobs(89)],'r:',[0 yobs(90)],[0 xobs(90)],'r:',...
%                         [0 yobs(91)],[0 xobs(91)],'r:',[0 yobs(92)],[0 xobs(92)],'r:',[0 yobs(93)],[0 xobs(93)],'r:',[0 yobs(94)],[0 xobs(94)],'r:',[0 yobs(95)],[0 xobs(95)],'r:',...
%                         [0 yobs(96)],[0 xobs(96)],'r:',[0 yobs(97)],[0 xobs(97)],'r:',[0 yobs(98)],[0 xobs(98)],'r:',[0 yobs(99)],[0 xobs(99)],'r:',[0 yobs(100)],[0 xobs(100)],'r:');
        
    axis equal
    title('x vs y');
    xlabel('y (km)')
    ylabel('x (km)')
    
     %---Plot 6: x vs. y (Line search filtered trajectory and QV with IC, QV-predicted states of Line search filtered at
    %   measurement times:) 
    figure(6)
    plot(x_ls_f(:,2),x_ls_f(:,1),'g','DisplayName','Line Filt');hold on;
    plot(x_vol(:,2),x_vol(:,1),'b','DisplayName','QV with IC');hold on;
    plot(x_vol_line_search_f(:,2,Imin_search_f),x_vol_line_search_f(:,1,Imin_search_f),'kx','MarkerSize',8,'DisplayName', 'Predicted States'); hold on;
    legend('Location','BestOutside');
    % plot([0 yobs(1)],[0 xobs(1)],'r:',[0 yobs(2)],[0 xobs(2)],'r:',[0 yobs(3)],[0 xobs(3)],'r:',[0 yobs(4)],[0 xobs(4)],'r:',[0 yobs(5)],[0 xobs(5)],'r:',[0 yobs(6)],[0 xobs(6)],'r:',[0 yobs(7)],[0 xobs(7)],'r:',[0 yobs(8)],[0 xobs(8)],'r:',[0 yobs(9)],[0 xobs(9)],'r:',[0 yobs(10)],[0 xobs(10)],'r:',[0 yobs(11)],[0 xobs(11)],'r:',[0 yobs(12)],[0 xobs(12)],'r:',[0 yobs(13)],[0 xobs(13)],'r:',[0 yobs(14)],[0 xobs(14)],'r:',[0 yobs(15)],[0 xobs(15)],'r:')
    plot([0 yobs(1)],[0 xobs(1)],'r:',[0 yobs(2)],[0 xobs(2)],'r:',[0 yobs(3)],[0 xobs(3)],'r:',[0 yobs(4)],[0 xobs(4)],'r:',[0 yobs(5)],[0 xobs(5)],'r:',[0 yobs(6)],[0 xobs(6)],'r:',[0 yobs(7)],[0 xobs(7)],'r:',[0 yobs(8)],[0 xobs(8)],'r:',[0 yobs(9)],[0 xobs(9)],'r:',[0 yobs(10)],[0 xobs(10)],'r:',[0 yobs(11)],[0 xobs(11)],'r:',[0 yobs(12)],[0 xobs(12)],'r:',[0 yobs(13)],[0 xobs(13)],'r:',[0 yobs(14)],[0 xobs(14)],'r:',...
           [0 yobs(15)],[0 xobs(15)],'r:');
%        [0 yobs(16)],[0 xobs(16)],'r:',[0 yobs(17)],[0 xobs(17)],'r:',[0 yobs(18)],[0 xobs(18)],'r:',[0 yobs(19)],[0 xobs(19)],'r:',[0 yobs(20)],[0 xobs(20)],'r:',...
%            [0 yobs(21)],[0 xobs(21)],'r:',[0 yobs(22)],[0 xobs(22)],'r:',[0 yobs(23)],[0 xobs(23)],'r:',[0 yobs(24)],[0 xobs(24)],'r:',[0 yobs(25)],[0 xobs(25)],'r:',...
%                         [0 yobs(26)],[0 xobs(26)],'r:',[0 yobs(27)],[0 xobs(27)],'r:',[0 yobs(28)],[0 xobs(28)],'r:',[0 yobs(29)],[0 xobs(29)],'r:',[0 yobs(30)],[0 xobs(30)],'r:',...
%                         [0 yobs(31)],[0 xobs(31)],'r:',[0 yobs(32)],[0 xobs(32)],'r:',[0 yobs(33)],[0 xobs(33)],'r:',[0 yobs(34)],[0 xobs(34)],'r:',[0 yobs(35)],[0 xobs(35)],'r:',...
%                         [0 yobs(36)],[0 xobs(36)],'r:',[0 yobs(37)],[0 xobs(37)],'r:',[0 yobs(38)],[0 xobs(38)],'r:',[0 yobs(39)],[0 xobs(39)],'r:',[0 yobs(40)],[0 xobs(40)],'r:',...
%                         [0 yobs(41)],[0 xobs(41)],'r:',[0 yobs(42)],[0 xobs(42)],'r:',[0 yobs(43)],[0 xobs(43)],'r:',[0 yobs(44)],[0 xobs(44)],'r:',[0 yobs(45)],[0 xobs(45)],'r:',...
%                         [0 yobs(46)],[0 xobs(46)],'r:',[0 yobs(47)],[0 xobs(47)],'r:',[0 yobs(48)],[0 xobs(48)],'r:',[0 yobs(49)],[0 xobs(49)],'r:',[0 yobs(50)],[0 xobs(50)],'r:',...
%                         [0 yobs(51)],[0 xobs(51)],'r:',[0 yobs(52)],[0 xobs(52)],'r:',[0 yobs(53)],[0 xobs(53)],'r:',[0 yobs(54)],[0 xobs(54)],'r:',[0 yobs(55)],[0 xobs(55)],'r:',...
%                         [0 yobs(56)],[0 xobs(56)],'r:',[0 yobs(57)],[0 xobs(57)],'r:',[0 yobs(58)],[0 xobs(58)],'r:',[0 yobs(59)],[0 xobs(59)],'r:',[0 yobs(60)],[0 xobs(60)],'r:',...
%                         [0 yobs(61)],[0 xobs(61)],'r:',[0 yobs(62)],[0 xobs(62)],'r:',[0 yobs(63)],[0 xobs(63)],'r:',[0 yobs(64)],[0 xobs(64)],'r:',[0 yobs(65)],[0 xobs(65)],'r:',...
%                         [0 yobs(66)],[0 xobs(66)],'r:',[0 yobs(67)],[0 xobs(67)],'r:',[0 yobs(68)],[0 xobs(68)],'r:',[0 yobs(69)],[0 xobs(69)],'r:',[0 yobs(70)],[0 xobs(70)],'r:',...
%                         [0 yobs(71)],[0 xobs(71)],'r:',[0 yobs(72)],[0 xobs(72)],'r:',[0 yobs(73)],[0 xobs(73)],'r:',[0 yobs(74)],[0 xobs(74)],'r:',[0 yobs(75)],[0 xobs(75)],'r:',...
%                         [0 yobs(76)],[0 xobs(76)],'r:',[0 yobs(77)],[0 xobs(77)],'r:',[0 yobs(78)],[0 xobs(78)],'r:',[0 yobs(79)],[0 xobs(79)],'r:',[0 yobs(80)],[0 xobs(80)],'r:',...
%                         [0 yobs(81)],[0 xobs(81)],'r:',[0 yobs(82)],[0 xobs(82)],'r:',[0 yobs(83)],[0 xobs(83)],'r:',[0 yobs(84)],[0 xobs(84)],'r:',[0 yobs(85)],[0 xobs(85)],'r:',...
%                         [0 yobs(86)],[0 xobs(86)],'r:',[0 yobs(87)],[0 xobs(87)],'r:',[0 yobs(88)],[0 xobs(88)],'r:',[0 yobs(89)],[0 xobs(89)],'r:',[0 yobs(90)],[0 xobs(90)],'r:',...
%                         [0 yobs(91)],[0 xobs(91)],'r:',[0 yobs(92)],[0 xobs(92)],'r:',[0 yobs(93)],[0 xobs(93)],'r:',[0 yobs(94)],[0 xobs(94)],'r:',[0 yobs(95)],[0 xobs(95)],'r:',...
%                         [0 yobs(96)],[0 xobs(96)],'r:',[0 yobs(97)],[0 xobs(97)],'r:',[0 yobs(98)],[0 xobs(98)],'r:',[0 yobs(99)],[0 xobs(99)],'r:',[0 yobs(100)],[0 xobs(100)],'r:');
    axis equal
    title('x vs y');
    xlabel('y (km)')
    ylabel('x (km)')
    
    %---Plot 7:  z vs. t (RK and QV with IC)
    figure(7)
    plot(trk,x(:,3),'b','DisplayName','2-Body RK w/IC');hold on;
    plot(trk,x_vol(:,3),'g','DisplayName','QV w/IC');
    legend('Location','BestOutside');
    title('z vs t');
    xlabel('t (sec)')
    ylabel('z (km)')
    
   %---Plot 8:  z vs. t (Mean unfiltered trajectory and QV with IC)
    figure(8)
    plot(trk,x_ma(:,3),'b','DisplayName','Mean Unfilt');hold on;
    plot(trk,x_vol(:,3),'g','DisplayName','QV w/IC');
    legend('Location','BestOutside');
    title('z vs t');
    xlabel('t (sec)')
    ylabel('z (km)')
    
   %---Plot 9:  z vs. t (Mean filtered trajectory and QV with IC)
    figure(9)
    plot(trk,x_ma_f(:,3),'b','DisplayName','Mean Filt');hold on;
    plot(trk,x_vol(:,3),'g','DisplayName','QV w/IC');
    legend('Location','BestOutside');
    title('z vs t');
    xlabel('t (sec)')
    ylabel('z (km)')
    
    %---Plot 10:  z vs. t (Line search unfiltered trajectory and QV with IC)
    figure(10)
    plot(trk,x_ls(:,3),'b','DisplayName','Line Unfilt');hold on;
    plot(trk,x_vol(:,3),'g','DisplayName','QV w/IC');
    legend('Location','BestOutside');
    title('z vs t');
    xlabel('t (sec)')
    ylabel('z (km)')
   
    %---Plot 11:  z vs. t (Line search filtered trajectory and QV with IC)
    figure(11)
    plot(trk,x_ls_f(:,3),'b','DisplayName','Line Filt');hold on;
    plot(trk,x_vol(:,3),'g','DisplayName','QV w/IC');
    legend('Location','BestOutside');
    title('z vs t');
    xlabel('t (sec)')
    ylabel('z (km)')
end

%---Plot 12:  RMS error for all 27 possible singular vectors
figure(12)
plot(RMSerr_SV,'DisplayName','unfiltered mean alpha');hold on;
plot(RMSerr_SV_filtered,'DisplayName','filtered mean alpha');hold on;
plot(minRMS_search,'DisplayName','unfiltered line search');hold on;
plot(minRMS_search_f,'DisplayName','filtered line search');hold on;
legend('Location','BestOutside');
grid on;grid minor;
title('RMS Error for 27 Singular Vectors');
xlabel('Singular Vector Number')
ylabel('Total RMS Error of Observation Angle Residuals (radians)')
% % % % 
print(figure(1),'SVDvsEIG_Results\Plot_All(500,15)_SVD_0C_NN','-dpng')
print(figure(2),'SVDvsEIG_Results\Plot_QV_R(500,15)_SVD_0C_NN','-dpng')
print(figure(3),'SVDvsEIG_Results\Plot_QV_MU(500,15)_SVD_0C_NN','-dpng')
print(figure(4),'SVDvsEIG_Results\Plot_QV_MF(500,15)_SVD_0C_NN','-dpng')
print(figure(5),'SVDvsEIG_Results\Plot_QV_LSU(500,15)_SVD_0C_NN','-dpng')
print(figure(6),'SVDvsEIG_Results\Plot_QV_LSF(500,15)_SVD_0C_NN','-dpng')
print(figure(7),'SVDvsEIG_Results\PlotZ_QV_RK(500,15)_SVD_0C_NN','-dpng')
print(figure(8),'SVDvsEIG_Results\PlotZ_QV_MU(500,15)_SVD_0C_NN','-dpng')
print(figure(9),'SVDvsEIG_Results\PlotZ_QV_MF(500,15)_SVD_0C_NN','-dpng')
print(figure(10),'SVDvsEIG_Results\PlotZ_QV_LSU(50,15)_SVD_0C_NN','-dpng')
print(figure(11),'SVDvsEIG_Results\PlotZ_QV_LSF(50,15)_SVD_0C_NN','-dpng')
print(figure(12),'SVDvsEIG_Results\Plot_Vectors(50,15)_SVD_0C_NN','-dpng')